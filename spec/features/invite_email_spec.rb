require 'rails_helper'
require 'helpers'


describe "Invitation Email" do
	include SpecHelper

	before(:each) do
		ActionMailer::Base.deliveries = []

		# Signup FirstUser
		user_id = sign_up_user()
		@userOne = User.find(user_id)

		#SecondUser, will be tested
		@secondUser = FactoryGirl.build(:user)

		party_id = user_creates_party()
		@party = Party.find(party_id)

		fill_in 'party_invitation_email', :with => @secondUser.email
		click_button 'Send'
		wait = Selenium::WebDriver::Wait.new(:timeout => 10) # seconds
		wait.until { page.body.include? @secondUser.email }
	end

	# This tests AJAX related content, needs Selenium or something.  Can't test this currently with CLoud9 workspace.
	# describe "First user has typed in second users email for invite" do
	# 	before do
	# 		# Sign out firstUser
	# 		click_link "Sign Out"
	# 	    visit root_path
	# 	end
	# 	it 'should create an outgoing email to the party', :js => true do
	# 		last_delivery = ActionMailer::Base.deliveries.last

	# 		expect(User.where(email:@secondUser.email)).not_to exist
	# 		expect(ActionMailer::Base.deliveries.size).to eq 1

	# 		#Link to the party
	# 		linkText = "party/" + PartyInvitation.last.party_id.to_s
	# 		expect(last_delivery).to have_content linkText
	# 	end
	# end

	after(:each) do
		driver = Capybara.current_session.driver
		driver.quit
	end

end