require 'rails_helper'
require 'helpers'

describe "User votes on an Entry" do
	include SpecHelper

	before(:all) do
		
		seed_disguise_table()
		
		#SecondUser, will be tested
		sign_up_user()
		# Sign out second User
		first(:link, "Sign Out").click
		@userOne = User.first
		
		# Signup FirstUser
		sign_up_user()
		@secondUser = User.last

		#Create party, hosted by userOne
		@party = FactoryGirl.create(:party, :user_id => @userOne.id, disguise_theme_id: DisguiseTheme.first.id)

		#Create membership
		# userOne already belongs since they created Party
		secondUserMembership = FactoryGirl.create(:party_membership, :user_id => @secondUser.id, :party_id => @party.id)

		#Create Entries for each user
		@userOneEntry = FactoryGirl.create(:entry, :user_id => @userOne.id, :party_id => @party.id)
		@secondUserEntry = FactoryGirl.create(:entry, :user_id => @secondUser.id, :party_id => @party.id)
	end

	it "(verify two users are created)" do
		expect(User.all.size).to eq 2
	end
	it "(verify party was created)" do
		expect(Party.first.user_id).to eq @userOne.id
	end
	it "(verify party memberships)" do
		expect(PartyMembership.all.size).to eq 2
	end
	it "(verify entries)" do
		expect(Entry.all.size).to eq 2
		expect(Entry.where(user_id: @userOne.id).first.disguise_id.nil?).to eq false
		expect(Entry.where(user_id: @secondUser.id).first.disguise_id.nil?).to eq false
	end

	describe "When User visits Party page" do
		before(:each) do
			# Visit SignIn page and login user
			visit '/users/sign_in'
			fill_in 'user_email', :with => @secondUser.email
		    fill_in 'user_password', :with => FactoryGirl.build(:user).password #default
		    click_button 'Log in'

			visit party_path(@party.id) #Link normally contained in the invite.

			# Shell votes are created
			@secondUserVote_userOneEntry = Vote.where(user_id: @secondUser.id, entry_id: @userOneEntry.id).first
			@secondUserVote_secondUserEntry = Vote.where(user_id: @secondUser.id, entry_id: @secondUserEntry.id).first
		end

		it "should have section for voting (both shell votes)" do
			expect(page).to have_content "Rate (not rank) other wines here..."

			expect(page).to have_content @userOneEntry.codename
			expect(page).to have_content @secondUserEntry.codename
		end

		it "should NOT have a link to Results (before party is concluded)" do
			expect(page).to_not have_link("Results")
		end

		
		it "should render update form" do
			find("#vote_score_" + @secondUserVote_userOneEntry.id.to_s, :visible => false)
			expect(page).to have_field("vote_note_" + @secondUserVote_userOneEntry.id.to_s, :type => 'textarea')

			find("#vote_score_" + @secondUserVote_secondUserEntry.id.to_s, :visible => false)
			expect(page).to have_field("vote_note_" + @secondUserVote_secondUserEntry.id.to_s, :type => 'textarea')
		end

		describe "submitting score" do
			before(:each) do
				@vote_note = "This is the note text"
				@vote_score = 2

				find('#vote_score_' + @secondUserVote_userOneEntry.id.to_s).find(:xpath, 'option[4]').select_option # option[4] == 2
			end

			# This tests AJAX related content, needs Selenium or something.  Can't test this currently with CLoud9 workspace.
			# it "should update the vote record", :js => true do
			# 	driver = Selenium::WebDriver.for :firefox
  	# 			driver.manage.timeouts.implicit_wait = 3

			# 	#Refresh the instance var
			# 	@secondUserVote_userOneEntry = Vote.find(@secondUserVote_userOneEntry.id)

			# 	expect(@secondUserVote_userOneEntry.score).to eq @vote_score
			# 	driver.quit
			# end
			
			# This tests AJAX related content, needs Selenium or something.  Can't test this currently with CLoud9 workspace.
			# describe "updating a vote for 2nd time - note" do
			# 	before(:each) do
			# 		@vote_note_updated = "This is the updated text"
			# 		@vote_score_updated = 3

			# 		fill_in 'vote_note_' + @secondUserVote_userOneEntry.id.to_s, :with => @vote_note_updated
			# 	end

			# 	it "should update the vote record - score", :js => true do
			# 		driver = Selenium::WebDriver.for :firefox
  	# 				driver.manage.timeouts.implicit_wait = 3

			# 		#Refresh the instance var
			# 		@secondUserVote_userOneEntry = Vote.find(@secondUserVote_userOneEntry.id)

			# 		expect(@secondUserVote_userOneEntry.note).to eq @vote_note_updated
			# 		driver.quit
			# 	end
			# end
		end

	end

end