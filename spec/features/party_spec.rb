require 'rails_helper'
require 'helpers'

#Tests Party model, associations.

describe "Party requests" do
	include SpecHelper

	before(:all) do
		seed_disguise_table()
		
		# Visit SignUp page and create User account
		user_id = sign_up_user()

		@firstUser = User.find(user_id)
		@user_email = @firstUser.email
		# Capture FactoryGirl:user default password
		@user_pwd = FactoryGirl.build(:user).password
		first(:link, "Sign Out").click
	end
	before(:each) do
		visit '/users/sign_in'
		fill_in 'user_email', :with => @user_email
	    fill_in 'user_password', :with => @user_pwd
	    click_button 'Log in'
	end


	describe 'A valid user' do
		it "(confirming that User was created)" do
			expect(User.where(email: @user_email)).to exist
		end


		#Testing associations here, because model associations 
		# user<->party are hard with Devise
		describe "can create a party (ActiveRecord test)" do
			before do
				@partyName = "Party123"
				sampleUser = User.where(email: @user_email).first
				@party = sampleUser.parties.create(name: @partyName, event_date: Time.now.to_s, disguise_theme_id: DisguiseTheme.first.id)
				@party.save
			end
			it "when party is valid" do
				expect(Party.where(name: @partyName)).to exist
				expect(PartyMembership.where(user_id: @firstUser.id)).to exist
			end

			describe "testing associations" do
				before do
					sampleUser = User.where(email: @user_email).first
					sampleUser.destroy
				end
				it "when User destroyed so should assoc. Party" do
					expect(Party.where(name: @partyName)).not_to exist
				end
			end
		end

		describe "routes and controllers for CRUD of Party" do
			before do
				visit '/party/new'
			end
			it "should be accessible to /parties/new" do
				expect(page).to have_content 'Start your party here'
			end

			describe "filling in form for /new" do
				before do
					@partyName = "Party123"
					@originalDate = "2014-01-31"
					fill_in 'party_name', :with => @partyName
					fill_in 'party_event_date', :with => @originalDate
					click_button 'Create Party'
				end
				it "should have created a Party" do
					expect(Party.where(name: @partyName)).to exist
					expect(page).to have_content @partyName
					expect(page).to have_content "Party created!" #Tests the flash content
				end
				describe "controller edit/update" do
					before do
						@newPartyDate = "2015-01-31"
						visit edit_party_path(Party.where(name: @partyName).first)
						fill_in 'party_event_date', :with => @newPartyDate
						click_button 'Update'
					end
					it "should update Party" do
						expect(page).to have_content @newPartyDate[0..3]
						expect(page).to have_content @partyName
					end

					describe "controller destroy" do
						before do
							visit party_path(Party.where(name: @partyName).first)
							click_link "Delete"
						end
						it "should delete the Party" do
							expect(Party.where(name: @partyName)).not_to exist
							expect(page).to have_content "Party deleted"
						end
					end

				end
			end
		end
	end
	
	scenario "Has logic to Conclude and Release Results properly" do
		# Verify user account
		expect(User.where(email: @user_email)).to exist	
		
		# Create Party
		visit '/party/new'
		expect(page).to have_content 'Start your party here'
		@partyName = "Party123"
		@originalDate = "2014-01-31"
		fill_in 'party_name', :with => @partyName
		fill_in 'party_event_date', :with => @originalDate
		click_button  'Create Party'
		#Verify
		expect(Party.where(name: @partyName)).to exist
		expect(page).to have_content @partyName
		expect(page).to have_content "Party created!" #Tests the flash content
		
		# Flip to concluded - unfortunatley the front end has JS/CSS, need to reach into the backend
		Party.where(name: @partyName).first.update(concluded: true)
		# Booleans should be set appropriately
		party = Party.first
		expect(party.concluded).to eq true
		expect(party.results_released).to eq false
		
		# Release Results - unfortunatley the front end has JS/CSS, need to reach into the backend
		Party.where(name: @partyName).first.update(results_released: true)
		# Booleans should be set appropriately
		party = Party.first
		expect(party.concluded).to eq true
		expect(party.results_released).to eq true
	end

end