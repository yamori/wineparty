require 'rails_helper'
require 'helpers'

describe "Invitation Email (sent to current user)" do
	include SpecHelper
	
	scenario "new user visits user#show page and makes edits" do
	   
        # Signup user
        user_name = "KimOfTheNorth"
        user_id = sign_up_user()
        @user = User.find(user_id)
        @user.update_attributes(name: user_name) # Ensure the user has a name
        # Verify default
        expect(@user.timezone).to eq "Eastern Time (US & Canada)"
        
        # The helper has already logged the user in.
        visit root_path
        expect(page).to have_content user_name # This will be the link to user's edit page
        
        first(:link, user_name).click
        expect(page).to have_content @user.email
        
        # Page should contain the form
        expect(page).to have_field("user_email")
        expect(page).to have_field("user_name")
        expect(page).to have_field("user_timezone")
        
        # Update values
        new_email = "bradely@comcast.net"
        new_name = "DougOfTheSouth"
        new_timezone = "Hawaii"
        fill_in 'user_email', with: new_email
        fill_in 'user_name', with: new_name
        select new_timezone, from: 'user_timezone'
        click_button 'Update Account'
        
        # Verify the changes
        @user = User.find(user_id)
        expect(@user.email).to eq new_email
        expect(@user.name).to eq new_name
        expect(@user.timezone).to eq new_timezone
	end
	
	scenario "existing user deletes account" do
	    
	    # Signup user
        user_name = "KimOfTheNorth"
        user_id = sign_up_user()
        @user = User.find(user_id)
        @user.update_attributes(name: user_name) # Ensure the user has a name
        
        # The helper has already logged the user in.
        visit root_path
        expect(page).to have_content user_name # This will be the link to user's edit page
        
        first(:link, user_name).click
        expect(page).to have_content @user.email
        expect(page).to have_link "Delete Account"
        
        # Delete account
        click_link 'Delete Account'
        expect(User.all.size).to eq 0
	end
	
end