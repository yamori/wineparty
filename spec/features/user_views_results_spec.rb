require 'rails_helper'
require 'helpers'

describe "User views results" do
	include SpecHelper

	# Host has party
	# joined by x Users
	# Everyone votes
	# Host ends party, logs out
	# User1 logs in
	# Navigates to party page, sees Results link
	# Clicks on Results link, sees results.

	# This feature is setup intensive (multiple users needed for entries and votes)
	#  -Host has party
	#  -x User join, all have entries, all vote
	#  -Host Concludes the party, is logged out

	before do
		
		seed_disguise_table()

		# Create users + host
		@userIDs = []
		@number_of_nonhost_members = 7
		@number_of_nonhost_members.times do
			sign_up_user()
			# Sign out User
			first(:link, "Sign Out").click
			@userIDs.push(User.last.id)
		end
		
		# Signup Host
		sign_up_user()
		@hostUser = User.last

		#Create party, hosted by Host
		@party = FactoryGirl.create(:party, :user_id => @hostUser.id, disguise_theme_id: DisguiseTheme.first.id)

		#Create memberships
		# hostUser already belongs since they created Party
		@userIDs.each do |userID|
			FactoryGirl.create(:party_membership, :user_id => userID, :party_id => @party.id)
		end

		#Create Entries for each user
		@entryIDs = []
		([@hostUser.id]|@userIDs).each do |userID|
			hostUserEntry = FactoryGirl.create(:entry, :user_id => userID, :party_id => @party.id)
			@entryIDs.push(hostUserEntry.id)
		end

		# Votes.  All except admin vote for all Entries
		@userIDs.each do |userID|
			@entryIDs.each do |entryID|
				FactoryGirl.create(:vote, :user_id => userID, :party_id => @party.id, :entry_id => entryID, :score => rand(11))
			end
		end

		# Host concludes the party and signs out
		visit party_path(@party.id)
		
		# Conclude ui/ux is javascript heavy, going through the backend.
		@party.flip_concluded
		
		visit party_path(@party.id)
		first(:link, "Sign Out").click

	end

	it "(verify three users are created)" do
		expect(User.all.size).to eq (@number_of_nonhost_members+1)
	end
	it "(verify party was created)" do
		expect(Party.first.user_id).to eq @hostUser.id
	end
	it "(verify party memberships)" do
		expect(PartyMembership.all.size).to eq (@number_of_nonhost_members+1)
	end
	it "(verify votes were created)" do
		expect(Vote.where(party_id: @party.id).where.not(score: nil).size).to eq ( (@number_of_nonhost_members+1)*@number_of_nonhost_members )
	end
	it "(verify party is concluded)" do
		expect(Party.find(@party.id).concluded).to eq true
	end

	describe "When User visits Party page" do
		before do
			# Visit SignIn page and login user
			visit '/users/sign_in'
			fill_in 'user_email', :with => User.find(@userIDs[0]).email
		    fill_in 'user_password', :with => FactoryGirl.build(:user).password #default
		    click_button 'Log in'

			visit party_path(@party.id) #Link normally contained in the invite.
		end

		it "should NOT have a link to Results (because party is concluded, but results not released)" do
			expect(page).to_not have_link("Results")
		end
		
		describe "When Host releases the results" do
			before do
				# User logs out, host logs in and releases party, user logs back in, views party page.
				first(:link, "Sign Out").click
				
				visit '/users/sign_in'
				fill_in 'user_email', :with => User.find(@hostUser.id).email
			    fill_in 'user_password', :with => FactoryGirl.build(:user).password #default
			    click_button 'Log in'
				visit party_path(@party.id) #Link normally contained in the invite.
				
				# Releasing results switch is css/js heavy, work aroudn for this rspec is to go to backend
				@party.update(results_released: true)
				visit party_path(@party.id)
				
				first(:link, "Sign Out").click
				
				visit '/users/sign_in'
				fill_in 'user_email', :with => User.find(@userIDs[0]).email
			    fill_in 'user_password', :with => FactoryGirl.build(:user).password #default
			    click_button 'Log in'
				visit party_path(@party.id) #Link normally contained in the invite.
			end
			
			it "should have a link to Results (because party is concluded, and results ARE released)" do
				expect(page).to have_link("See Results!")
			end
			
			describe "When User clicks link to See Results" do
				# before(:each) do
				# 	puts page.body
				# 	click_link "See Results!"
				# end
				before do
					first(:link, "See Results!").click
				end
	
				it "should go to the Results page" do
					expect(page).to have_content @party.name + " > Results"
					expect(page).to have_content "Final Averages - All Wines"
					expect(page).to have_content "How Users Voted"
				end
			end
		end
	end
end