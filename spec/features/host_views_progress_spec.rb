require 'rails_helper'
require 'helpers'

describe "Host adds codenames to Entries" do
	include SpecHelper

	# This feature is setup intensive (multiple users needed for entries and votes)

	before(:all) do
		
		seed_disguise_table()
		
		# Create three users

		# userOne
		sign_up_user()
		# Sign out second User
		first(:link, "Sign Out").click
		@userOne = User.last

		# userTwo
		sign_up_user()
		# Sign out second User
		first(:link, "Sign Out").click
		@userTwo = User.last
		
		# Signup FirstUser
		sign_up_user()
		@hostUser = User.last

		#Create party, hosted by userOne
		@party = FactoryGirl.create(:party, :user_id => @hostUser.id, disguise_theme_id: DisguiseTheme.first.id)

		#Create memberships
		# hostUser already belongs since they created Party
		hostUserMembership = FactoryGirl.create(:party_membership, :user_id => @userOne.id, :party_id => @party.id)
		hostUserMembership = FactoryGirl.create(:party_membership, :user_id => @userTwo.id, :party_id => @party.id)

		#Create Entries for each user
		@hostUserEntry = FactoryGirl.create(:entry, :user_id => @hostUser.id, :party_id => @party.id)
		@userOneEntry = FactoryGirl.create(:entry, :user_id => @userOne.id, :party_id => @party.id)
		@userTwoEntry = FactoryGirl.create(:entry, :user_id => @userTwo.id, :party_id => @party.id)
	end

	it "(verify three users are created)" do
		expect(User.all.size).to eq 3
	end
	it "(verify party was created)" do
		expect(Party.first.user_id).to eq @hostUser.id
	end
	it "(verify party memberships)" do
		expect(PartyMembership.all.size).to eq 3
	end
	it "(verify entries)" do
		expect(Entry.all.size).to eq 3
		expect(Entry.where(user_id: @hostUser.id).first.disguise_id.nil?).to eq false
		expect(Entry.where(user_id: @userOne.id).first.disguise_id.nil?).to eq false
		expect(Entry.where(user_id: @userTwo.id).first.disguise_id.nil?).to eq false
	end

	describe "When User visits Party page" do
		before(:each) do
			# Visit SignIn page and login user
			visit '/users/sign_in'
			fill_in 'user_email', :with => @hostUser.email
		    fill_in 'user_password', :with => FactoryGirl.build(:user).password #default
		    click_button 'Log in'

			visit party_path(@party.id) #Link normally contained in the invite.
		end
		
		it "should have the Social Dashboard link" do
			expect(page).to have_content "Social Dashboard"
		end

		it "should have a link to check the progress of the party" do
			expect(page).to have_link("Check Everyone's Progress")
			# This is an ajax call and probably needs some tests
		end

	end

end