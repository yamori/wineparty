require 'rails_helper'
require 'helpers'

describe "User adds Entry to Party" do
	include SpecHelper

	before(:each) do
		# Signup FirstUser
		user_id = sign_up_user()
		@userOne = User.find(user_id)

		# First user creates a party
		@party_id = user_creates_party()
		@party = Party.find(@party_id)
	end

	describe "When User visit Party for first time" do
		before do
			visit party_path(@party.id) #Link normally contained in the invite.
		end
		it "should prompt for user to enter Entry clearname" do
			expect(page).to have_content "(You still need to add your wine bottle)"
		end

		describe "when user's Entry is given clearname clearname (ajax functionality, here backended)" do
			before do
				@clearname1 = "entryaname1"
				@userOne.entries.create(party_id: @party_id, clearname: @clearname1)
				visit party_path(@party.id)
			end
			it "should create an Entry record" do
				expect(Entry.all.size).to eq 1
				expect(Entry.first.clearname).to eq @clearname1
				expect(find_field('clearname_textfield2').value).to eq @clearname1
			end

		end
	end

end