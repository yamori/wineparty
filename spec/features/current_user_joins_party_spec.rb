require 'rails_helper'
require 'helpers'

describe "Invitation Email (sent to current user)" do
	include SpecHelper

	before(:each) do
		#SecondUser, will be tested
		@userOne = User.find( sign_up_user() )
		# Sign out second User
		first(:link, "Sign Out").click
		@secondUser = User.last
		
		# Signup FirstUser
		sign_up_user()
	end

	it "(verify two users are created)" do
		expect(User.all.size).to eq 2
	end

	describe "First user sends invite" do
		before do
		    # First user creates a party
			party_id = user_creates_party()
			@party = Party.find(party_id)

			# Create an invitation directed at User2
			invite = PartyInvitation.create(email: @secondUser.email, party_id: @party.id)

			# Sign out firstUser
			first(:link, "Sign Out").click
		    visit party_path(@party.id) #Link normally contained in the invite.
		end

		it "should ask the user to sign in" do
			expect(page).to have_content "You need to sign in or sign up before continuing"
		end

		describe "Second user signs in" do
			before do
				fill_in 'user_email', :with => @secondUser.email
			    fill_in 'user_password', :with => FactoryGirl.build(:user).password #default
			    click_button 'Log in'
			end
			it "should display invitation to Party" do
				expect(PartyInvitation.all.size).to eq 1
				expect(page).to have_content "Invitations (" + PartyInvitation.all.size.to_s + ")"
				expect(page).to have_content @party.name
			end
			
			describe "Second user accpets invitation" do
				before do
					first(:button, "Accept").click
					first(:link, "Sign Out").click
				end
				
				it "should remove the invite" do
					expect(PartyInvitation.all.size).to eq 0
				end
				
				describe "First user logs back and views party" do
					before do
						visit party_path(@party.id)
						fill_in 'user_email', :with => @userOne.email
					    fill_in 'user_password', :with => FactoryGirl.build(:user).password #default
					    click_button 'Log in'
					end
					
					it "should be able to view that first user is listed as 'accepted'" do 
						expect(page).to have_content @userOne.email
					end
				end
			end
		end
	end

end
