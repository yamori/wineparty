#require 'spec_helper'
require 'rails_helper'
require 'helpers'


describe "Authentication" do
	include SpecHelper

	before(:all) do
		# Visit SignUp page and create User account
		user_id = sign_up_user()

		@firstUser = User.find(user_id)
		@user_email = @firstUser.email
		# Capture FactoryGirl:user default password
		@user_pwd = FactoryGirl.build(:user).password
		first(:link, "Sign Out").click
	end
	before(:each) do
		# Visit SignIn page and login user
		visit '/users/sign_in'
		fill_in 'user_email', :with => @user_email
	    fill_in 'user_password', :with => @user_pwd
	    click_button 'Log in'
	end


	describe 'Creating a User account' do

		before do
		    visit root_path
		end
		it 'with valid email' do
			expect(User.where(email:@user_email)).to exist
		end

		describe "as signed in user" do
			before do
				visit root_path
				first(:link, "Sign Out").click
			end
			it 'should allow signout' do
				expect(page).to have_content "Signed out!"
			end
		end
	end

	describe 'Fail to create a User account' do
		before do
			first(:link, "Sign Out").click #Because before(:each) has a user signed in for this spec
			@invalid_user_email = 'userexample.com'
			user_pwd = 'foobar12'
			visit '/users/sign_up'
			fill_in 'user_email', :with => @invalid_user_email
			fill_in 'user_password', :with => user_pwd
			fill_in 'user_password_confirmation', :with => user_pwd
			click_button 'Sign up'
		end
		it 'with invalid email' do
			expect(User.where(email:@invalid_user_email)).not_to exist
		end
	end

	describe "Authentication to Party routes" do

		let(:sampleParty) { FactoryGirl.create(:party,:user_id => @firstUser.id, :name => "Woohoo") }
		it "(confirm first users Party)" do
			visit party_path(sampleParty)
			expect(Party.where(name:sampleParty.name)).to exist
		end

		before(:each) do
			first(:link, "Sign Out").click #Because before(:each) has a user signed in for this spec
			@secondUser_email = 'secondUser@example.com'
			user_pwd = 'foobar12'
			visit '/users/sign_up'
			fill_in 'user_email', :with => @secondUser_email
			fill_in 'user_password', :with => user_pwd
			fill_in 'user_password_confirmation', :with => user_pwd
			click_button 'Sign up'
		end

		describe "attempting access to party#show as wrong user" do
			before do
				visit party_path(sampleParty)
			end
			it "should not allow access" do
				expect(page).to have_content "Something went wrong"
			end
		end

		describe "attempting access to party#edit as wrong user" do
			before do
				visit edit_party_path(sampleParty)
			end
			it "should not allow access" do
				expect(page).to have_content "Something went wrong"
			end
		end

	end

end