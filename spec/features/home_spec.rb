#require 'spec_helper'
require 'rails_helper'

describe 'Visit home page' do
  it 'displays the site name' do
    visit '/'
    expect(page).to have_content 'Grape.Social'
  end
end