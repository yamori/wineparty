require 'rails_helper'
require 'helpers'

describe "User views their party page way before the event_date" do
	include SpecHelper
	
	before do
	    seed_disguise_table()

		sign_up_user()
		@hostUser = User.last
		
		#Create party, hosted by userOne
		distant_date = Time.now + 8.days
		@party = FactoryGirl.create(:party, :user_id => @hostUser.id, disguise_theme_id: DisguiseTheme.first.id, event_date: distant_date)
	    
	    visit party_path(@party.id) # Still as the hostUser
	end
	
	it "(verify party was created)" do
	    expect(Party.first.user_id).to eq @hostUser.id
	end
	
	it "(verify party page)" do
	    expect(page).to have_content @party.name
	end
	
	it "should not show the usual party controls" do
	    # These are the two labels for the switch to control a party
	    expect(page).not_to have_content "Tasting In Progress"
	    expect(page).not_to have_content "Everyone's Done"
	end
	
	it "should show some text: 'Party is more than a day away' " do
	    expect(page).to have_content "The party is more than a day away."
	end
	
	describe "changing the event_date to within 35 hours" do
	    
	    before do
	        @party.update(event_date: Time.now + 35.hours)
	        visit party_path(@party.id) # Still as the hostUser
	    end
	    
	    it "should not show the usual party controls" do
    	    # These are the two labels for the switch to control a party
    	    expect(page).to have_content "Tasting In Progress"
    	    expect(page).to have_content "Everyone's Done"
    	end
    	
    	it "should show some text: 'Party is more than a day away' " do
    	    expect(page).not_to have_content "The party is more than a day away."
    	end
	    
	end
end