require 'rails_helper'
require 'helpers'

describe "Party Invitations" do
  include SpecHelper

  before(:each) do
    sign_up_user()

    user_creates_party()
  end
 
 # This tests AJAX related content, needs Selenium or something.  Can't test this currently with CLoud9 workspace.
  # describe "valid email" do
  #   before do
  #     @inviteEmail = "inviteEmail@hotmail.com"
  #     #Invite, ajax call
  #     fill_in "party_invitation_email", with: @inviteEmail
  #     click_button "Send"
  #   end
  #   it "captures (downcased) email and displays email invite", :js => true do
  #     expect(page).to have_content "Pending Invitations (1)"
  #     expect(PartyInvitation.last.email).to eq @inviteEmail.downcase
  #   end
  # end

  # describe "invalid email" do
  #   before do
  #     @inviteEmail = "invalidemail"
  #     #Invite, ajax call
  #     fill_in "party_invitation_email", with: @inviteEmail
  #     click_button "Send"
  #   end
  #   it "does not captures and display INVALID email invite", :js => true do
  #     expect(page).not_to have_content @inviteEmail
  #     expect(page).to have_content "Sorry, the email didn't look valid"
  #   end
  # end

  after(:each) do
    driver = Capybara.current_session.driver
    driver.quit
  end
 
end