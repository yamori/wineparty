require 'rails_helper'
require 'helpers'

describe "Host adds codenames to Entries" do
	include SpecHelper

	# Host clicks button "end party"
	#   flips boolean for @party
	#   displays link for 'present results'.  (also have view results, tailored for individual viewing)
	#   update links for voting are not present
	# Host click 'present results' link
	#  sees various content: First to vote, First to finish, Lowest vote for own entry, 
	#   highest for own entry, bottom 3 (if more than three guests), Top 3, final scores and ranked.


	# This feature is setup intensive (multiple users needed for entries and votes)

	before(:all) do
		
		seed_disguise_table()

		# Create users + host
		@userIDs = []
		@number_of_nonhost_members = 7
		@number_of_nonhost_members.times do
			sign_up_user()
			# Sign out User
			first(:link, "Sign Out").click
			@userIDs.push(User.last.id)
		end
		
		# Signup Host
		sign_up_user()
		@hostUser = User.last
		first(:link, "Sign Out").click

		#Create party, hosted by Host
		@party = FactoryGirl.create(:party, :user_id => @hostUser.id, disguise_theme_id: DisguiseTheme.first.id)

		#Create memberships
		# hostUser already belongs since they created Party
		@userIDs.each do |userID|
			FactoryGirl.create(:party_membership, :user_id => userID, :party_id => @party.id)
		end

		#Create Entries for each user
		@entryIDs = []
		([@hostUser.id]|@userIDs).each do |userID|
			hostUserEntry = FactoryGirl.create(:entry, :user_id => userID, :party_id => @party.id)
			@entryIDs.push(hostUserEntry.id)
		end

		# Votes.  All except host vote for all Entries
		@userIDs.each do |userID|
			@entryIDs.each do |entryID|
				FactoryGirl.create(:vote, :user_id => userID, :party_id => @party.id, :entry_id => entryID, :score => rand(11))
			end
		end

	end

	it "(verify three users are created)" do
		expect(User.all.size).to eq (@number_of_nonhost_members+1)
	end
	it "(verify party was created)" do
		expect(Party.first.user_id).to eq @hostUser.id
	end
	it "(verify party memberships)" do
		expect(PartyMembership.all.size).to eq (@number_of_nonhost_members+1)
	end
	it "(verify votes were created)" do
		expect(Vote.where(party_id: @party.id).where.not(score: nil).size).to eq ( (@number_of_nonhost_members+1)*@number_of_nonhost_members )
	end

	describe "When User visits Party page" do
		before(:each) do
			# Visit SignIn page and login user
			visit '/users/sign_in'
			fill_in 'user_email', :with => @hostUser.email
		    fill_in 'user_password', :with => FactoryGirl.build(:user).password #default
		    click_button 'Log in'

			visit party_path(@party.id) #Link normally contained in the invite.
		end

		it "should have a button to conclude the party" do
			expect(page).to have_css("div#party_concluded_switch")
		end

		describe "When Host User clicks to Conclude the Party" do
			before(:all) do
				# The concluded button/switch is js which isn't supported by the
				#  current rspec browser/set
				@party.flip_concluded
				visit party_path(@party.id)
			end
			it "should conclude the party" do
				expect(Party.find(@party.id).concluded).to eq true
				expect(page).to have_content "Present Results (this is fun!)"
			end
			it "should have a button to Resume the party" do
				# This switch bot concludes and resumes
				expect(page).to have_css("div#party_concluded_switch")
			end
			it "should display the link to Present results" do
				expect(Party.find(@party.id).concluded).to eq true
				expect(page).to have_link("View Results")
			end

			# SHould test the 'VIew Results' link here, but the modal is
			#  js heavy.
		end


	end

end