require 'rails_helper'
require 'helpers'


describe "Invitation Email (sent to new user)" do
	include SpecHelper

	before(:each) do
		# Signup FirstUser
		user_id = sign_up_user()
		@userOne = User.find(user_id)

		#SecondUser, will be tested
		@secondUser = FactoryGirl.build(:user)

		# First user creates a party
		party_id = user_creates_party()
		@party = Party.find(party_id)

		# Create an invitation directed at User2
		invite = PartyInvitation.create(email: @secondUser.email, party_id: @party.id)
	end

	describe "When a current user visits an invitation link" do
		before do
			# Sign out firstUser
			first(:link, "Sign Out").click
		    visit party_path(@party.id) #Link normally contained in the invite.
		end

		it "should ask the user to sign in" do
			expect(page).to have_content "You need to sign in or sign up before continuing"
		end

		describe "User has signed in" do

			before do
				click_link "Sign up"
				fill_in 'user_email', :with => @secondUser.email
			    fill_in 'user_password', :with => @secondUser.password
			    fill_in 'user_password_confirmation', :with => @secondUser.password
			    click_button 'Sign up'
			end
			it "should display invitation to Party" do
				expect(page).to have_content "Invitations (" + PartyInvitation.all.size.to_s + ")"
				expect(page).to have_content @party.name
			end

			describe "User has accepted invite" do

				before do
					#Multiple forms are rendered for mobile vs. Desktop
					click_button("Accept", :match => :first)
				end
				it "should show the Party page" do
					expect(page).to have_content @party.name
					# Should also cleanup the invite.
					expect(PartyInvitation.where(user_id: @secondUser.id).size).to eq 0
					expect(User.where(email: @secondUser.email).first.party_memberships.size).to eq 1
				end
				it "should have Shout form" do
					expect(page).to have_content "Give the Party a Shout!"
				end
				it "should NOT have the Social Dashboard link" do
					expect(page).not_to have_content "Social Dashboard"
				end
			end
		end

	end

end