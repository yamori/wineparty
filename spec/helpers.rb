module SpecHelper
  def sign_up_user
  	# Build and Sign Up a new User, return user.id
    @user = FactoryGirl.build(:user) #Not saved
    userEmail = @user.email
    userPwd = @user.password
    visit '/users/sign_up'
    fill_in 'user_email', :with => userEmail
    fill_in 'user_password', :with => userPwd
    fill_in 'user_password_confirmation', :with => userPwd
    click_button 'Sign up'
    return User.where(email: userEmail).first.id
  end 

  def user_creates_party
    # For any party related Features, need to populate the Disguise and DisguiseTheme tables
    seed_disguise_table()
    
  	#After method "sign_up_user", or when user is logged in
  	# Return party.id
  	@sampleParty = FactoryGirl.build(:party)
    visit new_party_path
    fill_in "party_name", with: @sampleParty.name
    fill_in "party_event_date", with: @sampleParty.event_date.strftime("%F")
    # Use the default Disguise theme, i.e. don't change what is already selected at render
    click_button "Create Party"
    return Party.where(name: @sampleParty.name).first.id
  end
  
  def seed_disguise_table
    # The Disguise and DisguiseTables need some entries for Party model relations to work
    disguiseTheme1 = DisguiseTheme.create(name: "Category1")
    30.times do |n|
      Disguise.create(disguise_theme_id: disguiseTheme1.id, name: "DisguiseName1stTheme#{n}")
    end
    
    disguiseTheme2 = DisguiseTheme.create(name: "Category2")
    30.times do |n|
      Disguise.create(disguise_theme_id: disguiseTheme2.id, name: "DisguiseName2ndTheme#{n}")
    end
  end
end