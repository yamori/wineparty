FactoryGirl.define do  
  
  factory :disguise do
    name "MyString"
    disguise_theme_id 1
  end
  
  factory :disguise_theme do
    name "MyString"
  end
  
  factory :shout do
    user_id 1
    party_id 1
    content "MyString"
  end
  

  factory :wine do
    name "Wine Merlot 2011"
  end

  factory :vote do
    user_id -1
    party_id -1
    entry_id -1
    score 10
    note "VoteNote"
  end

  factory :entry do
    user_id -1
    party_id -1
    sequence(:clearname) { |n| "clearname#{n}" }
  end

  factory :party_membership do
    party_id 1
    user_id 1
  end
  

  factory :party_invitation do |invite|
    sequence(:email) { |n| "inviteEmail#{n}@example.com" }
    invite.user_id -1
    invite.party_id -1
  end

  factory :user do |user|
    sequence(:email) { |n| "person#{n}@example.com" }
    user.password               "password"
    user.password_confirmation  "password"
  end

  factory :party do |party|
    sequence(:name) { |n| "Party#{n}" }
  	party.event_date			  Time.now
  	party.user_id				    10
  	party.disguise_theme_id  1
  end

end