require 'rails_helper'

RSpec.describe Entry, :type => :model do
    
    # THis rspec describes all the model logic connecting Entry and Disguise.
    
    scenario "must automatically be assigned a disguise_id upon creation" do
        # Setup the disguises
        disguiseTheme = FactoryGirl.create(:disguise_theme)
        disguise = FactoryGirl.create(:disguise, disguise_theme_id: disguiseTheme.id)
        
        # Setup the party
        host = FactoryGirl.create(:user)
        party = FactoryGirl.create(:party, user_id: host.id, disguise_theme_id: disguiseTheme.id)
        
        # Add the entry
        entry = FactoryGirl.build(:entry, user_id: host.id, party_id: party.id)
        expect(entry).to be_valid
        entry.save
        expect(entry.disguise_id).to eq disguise.id
    end
    
    scenario "entries logic will use all disguises available" do
        nmbrOfEntries = 10
        # Setup the disguises
        disguiseTheme = FactoryGirl.create(:disguise_theme)
        nmbrOfEntries.times do |n|
            disguise = FactoryGirl.create(:disguise, name: "disguise#{n}", disguise_theme_id: disguiseTheme.id)
        end
        # Verify
        expect(Disguise.all.size).to eq nmbrOfEntries
        
        # Setup the party
        host = FactoryGirl.create(:user)
        party = FactoryGirl.create(:party, user_id: host.id, disguise_theme_id: disguiseTheme.id)
        
        # Add the party attendees and their entries
        nmbrOfEntries.times do |n|
            user = FactoryGirl.create(:user, email: "email#{n}@gmail.com")
            FactoryGirl.create(:party_membership, party_id: party.id, user_id: user.id)
            entry = FactoryGirl.create(:entry, user_id: user.id, party_id: party.id)
        end
        # Verify
        expect(Entry.all.size).to eq nmbrOfEntries
        
        # Validate, all disguises have been used
        allDisguiseIDs = Disguise.all.pluck(:id)
        allEntriesDisguiseIDs = Entry.all.pluck(:disguise_id)
        expect(allDisguiseIDs.uniq.size).to eq allEntriesDisguiseIDs.uniq.size
        expect( (allDisguiseIDs - allEntriesDisguiseIDs).size ).to eq 0
    end
    
    scenario "when all disguises are used, the +1 must come from a different theme" do
        # Setup the disguises
        nmbrOfPrimaryEntries = 3
        disguiseTheme = FactoryGirl.create(:disguise_theme, name: "primary")
        nmbrOfPrimaryEntries.times do |n|
            disguise = FactoryGirl.create(:disguise, name: "disguise#{n}", disguise_theme_id: disguiseTheme.id)
        end
        secondDisguiseTheme = FactoryGirl.create(:disguise_theme, name: "secondary")
        secondaryDisguise = FactoryGirl.create(:disguise, name: "disguise#{nmbrOfPrimaryEntries+1}", disguise_theme_id: secondDisguiseTheme.id)
        # Verify
        expect(Disguise.all.size).to eq nmbrOfPrimaryEntries+1
        
        # Create a party, attendees, and their entries
        # Setup the party
        host = FactoryGirl.create(:user)
        party = FactoryGirl.create(:party, user_id: host.id, disguise_theme_id: disguiseTheme.id)
        
        # Add the party attendees and their entries
        nmbrOfPrimaryEntries.times do |n|
            user = FactoryGirl.create(:user, email: "email#{n}@gmail.com")
            FactoryGirl.create(:party_membership, party_id: party.id, user_id: user.id)
            entry = FactoryGirl.create(:entry, user_id: user.id, party_id: party.id)
        end
        # Verify
        expect(Entry.all.size).to eq nmbrOfPrimaryEntries
        
        # This +1 entry should be assigned to the secondaryDisguise
        user = FactoryGirl.create(:user, email: "email#{nmbrOfPrimaryEntries+1}@gmail.com")
        FactoryGirl.create(:party_membership, party_id: party.id, user_id: user.id)
        entryPlusOne = FactoryGirl.create(:entry, user_id: user.id, party_id: party.id)
        expect(entryPlusOne.disguise_id).to eq secondaryDisguise.id
    end
    
end