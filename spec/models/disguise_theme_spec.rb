# == Schema Information
#
# Table name: disguise_themes
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe DisguiseTheme, :type => :model do
    
    scenario "requires name" do
        theme = FactoryGirl.build(:disguise_theme, name: nil)
        expect(theme).not_to be_valid
    end
    
    scenario "name must be >=5 chars" do
        theme = FactoryGirl.build(:disguise_theme, name: "x"*4)
        expect(theme).not_to be_valid
        theme.name = "x"*5
        expect(theme).to be_valid
    end
    
    scenario "name must be unique" do
        category = "Category123"
        theme1 = FactoryGirl.create(:disguise_theme, name: category)
        theme2 = FactoryGirl.build(:disguise_theme, name: category)
        expect(theme2).not_to be_valid
    end
    
    scenario "has many (associated) disguises" do
        theme1 = FactoryGirl.create(:disguise_theme)
        expect(DisguiseTheme.all.size).to eq 1 # Verify
        
        disguise1 = FactoryGirl.create(:disguise, name: "123", disguise_theme_id: theme1.id)
        disguise2 = FactoryGirl.create(:disguise, name: "456", disguise_theme_id: theme1.id)
        expect(Disguise.all.size).to eq 2 # Verify
        
        # Check the association
        expect(theme1.disguises.size).to eq 2
    end
    
    scenario "upon destroy, deletes associated disguises" do
        theme1 = FactoryGirl.create(:disguise_theme)
        expect(DisguiseTheme.all.size).to eq 1 # Verify
        
        disguise1 = FactoryGirl.create(:disguise, name: "123", disguise_theme_id: theme1.id)
        disguise2 = FactoryGirl.create(:disguise, name: "456", disguise_theme_id: theme1.id)
        expect(Disguise.all.size).to eq 2 # Verify
        expect(theme1.disguises.size).to eq 2 # Verify
        
        # Destroy then check
        theme1.destroy
        expect(DisguiseTheme.all.size).to eq 0
        expect(Disguise.all.size).to eq 0
    end
end
