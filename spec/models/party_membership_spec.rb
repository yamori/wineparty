# == Schema Information
#
# Table name: party_memberships
#
#  id         :integer          not null, primary key
#  party_id   :integer
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe PartyMembership, :type => :model do

	describe "PartyMembership model" do

		let!(:user) { FactoryGirl.create(:user) }
		let!(:sampleParty) { FactoryGirl.create(:party, :user_id => user.id) }

		it "should have an associated membership" do
			expect(PartyMembership.all.size).to   eq 1
		end

		describe "Destroying a party" do
			before do
				sampleParty.destroy
			end
			it "should also destory membership" do
				expect(Party.all.size).to   eq 0
				expect(PartyMembership.all.size).to   eq 0
			end
		end

		describe "Destroying a user" do
			before do
				user.destroy
			end
			it "should also destory membership" do
				expect(User.all.size).to   eq 0
				expect(PartyMembership.all.size).to   eq 0
			end
		end

	end
end
