require 'rails_helper'
require 'helpers'
include SpecHelper

RSpec.describe Party, :type => :model do
    
    scenario "party's theme updated results in entries gettings new disguises" do
        
        seed_disguise_table()
        disguiseThemeIDs = DisguiseTheme.all.pluck(:id)
        
        # Users 
        host = FactoryGirl.create(:user)
        attendee = FactoryGirl.create(:user)
        
        # Party - Assign first DisgusieTheme
        @party = FactoryGirl.create(:party, user_id: host.id, disguise_theme_id: disguiseThemeIDs[0])
        
        # Membership
        FactoryGirl.create(:party_membership, :user_id => host.id, :party_id => @party.id)
		FactoryGirl.create(:party_membership, :user_id => attendee.id, :party_id => @party.id)
		
		# Entries
		FactoryGirl.create(:entry, :user_id => host.id, :party_id => @party.id)
		FactoryGirl.create(:entry, :user_id => attendee.id, :party_id => @party.id)
		
		# (Verify)
		expect(User.all.size).to eq 2
		expect(Entry.all.size).to eq 2
        # Utilized disguises should only be from Disguisetheme[0]
        expect( (Entry.all.pluck(:disguise_id) - Disguise.where(disguise_theme_id: disguiseThemeIDs[0]).pluck(:id)).size ).to eq 0
        
        # Update the Party's theme
        @party.update_attributes( disguise_theme_id: disguiseThemeIDs[1] )
        
        # Test
        #   Utilized disguises should only be from Disguisetheme[1]
        expect( (Entry.all.pluck(:disguise_id) - Disguise.where(disguise_theme_id: disguiseThemeIDs[1]).pluck(:id)).size ).to eq 0
        
    end
    
end