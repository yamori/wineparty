# == Schema Information
#
# Table name: party_invitations
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  party_id   :integer
#  email      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe PartyInvitation, :type => :model do
  
  describe "PartyInvitation model" do

    let!(:user) { FactoryGirl.create(:user) }
    let!(:invited_user) { FactoryGirl.create(:user) }
  	let!(:sampleParty) { FactoryGirl.create(:party,:user_id => user.id) }
  	let!(:invite) { FactoryGirl.create(:party_invitation,:user_id => invited_user.id,:party_id => sampleParty.id) }

  	it "should be deleted when associated Party is deleted" do
  		expect { sampleParty.destroy }.to change(PartyInvitation, :count).by(-1)
  	end

  	it "should be deleted when associated User is deleted" do
  		expect { user.destroy }.to change(PartyInvitation, :count).by(-1)
  	end

  	describe "auto detect existing User" do
  		let!(:secondUser) { FactoryGirl.create(:user) }
	  	let!(:secondInvite) { FactoryGirl.create(:party_invitation,:email => secondUser.email,:party_id => sampleParty.id) }
	  	it "should automatically grab user.id" do
	  		expect(secondInvite.user_id).to   eq(secondUser.id)
	  	end
  	end

    describe "emailed notification" do
      it "should send an email notification" do
        expect { invite.save }.to change { ActionMailer::Base.deliveries.count }.by(1)
      end
    end


  end
end
