# == Schema Information
#
# Table name: wines
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Wine, :type => :model do
  
	let!(:existing_wine) { FactoryGirl.create(:wine) }
	let!(:candidate_wine) { FactoryGirl.build(:wine) }

	it "name should not be blank" do
		candidate_wine.name = nil
		expect(candidate_wine).not_to be_valid
	end

	it "name should be unique" do
		candidate_wine.name = existing_wine.name
		expect(candidate_wine).not_to be_valid
	end

end
