# == Schema Information
#
# Table name: disguises
#
#  id                :integer          not null, primary key
#  name              :string
#  disguise_theme_id :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

require 'rails_helper'

RSpec.describe Disguise, :type => :model do

    scenario "requires name" do
        disguise = FactoryGirl.build(:disguise, name: nil)
        expect(disguise).not_to be_valid
    end
    
    scenario "name must be >=3 chars" do
        disguise = FactoryGirl.build(:disguise, name: "x"*2)
        expect(disguise).not_to be_valid
        disguise = FactoryGirl.build(:disguise, name: "x"*3)
        expect(disguise).to be_valid
    end
    
    scenario "requires disguise_theme_id" do
        disguise = FactoryGirl.build(:disguise, disguise_theme_id: nil)
        expect(disguise).not_to be_valid
    end
    
    scenario "name must be unique per theme (case insensitive)" do
        dName = "a"*6
        themID = 1
        disguise1 = FactoryGirl.create(:disguise, name: dName, disguise_theme_id: themID)
        disguise2 = FactoryGirl.build(:disguise, name: dName.upcase, disguise_theme_id: themID)
        expect(disguise2).not_to be_valid
    end
end
