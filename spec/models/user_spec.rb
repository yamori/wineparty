# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default("0"), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime
#  updated_at             :datetime
#  provider               :string
#  uid                    :string
#  name                   :string
#  timezone               :string
#

require 'rails_helper'

RSpec.describe User, :type => :model do
    
    scenario "requires email" do
        user = User.new(email: nil, password: 'x'*9)
        expect(user).not_to be_valid # Required to attach error messages below.
        expect(user.errors.messages.size).to eq 1
        expect(user.errors.messages.key?(:email)).to eq true
    end
    
    scenario "allows nil or empty name" do
        user = User.new(email: "abc@gmail.com", password: 'x'*9)
        expect(user).to be_valid
        # Change user.name to exmpty
        user.name = ""
        expect(user).to be_valid
    end
    
    scenario "name must be >=6 chars" do
        user = User.new(email: "abc@gmail.com", password: 'x'*9, name: "x"*5)
        expect(user).not_to be_valid
        expect(user.errors.messages.size).to eq 1
        expect(user.errors.messages.key?(:name_length)).to eq true
        
        # Change to 6 chars
        user.name = "x"*6
        expect(user).to be_valid
    end
    
    scenario "has a method determining if a name is needed" do
        user = User.new(email: "abc@gmail.com", password: 'x'*9, name: "x"*5)
        expect(user.needs_name).to eq true
        user.name=nil
        expect(user.needs_name).to eq true
        user.name="x"*6
        expect(user.needs_name).to eq false
    end
    
    scenario "has timezone, and defaults to Eastern Time (US & Canada)" do
        # Create a user
        user = User.new(email: "abc@gmail.com", password: 'x'*9, name: "x"*8)
        
        expect(user.timezone).to eq "Eastern Time (US & Canada)"
    end
    
    scenario "requires timezone" do
        # Create a user
        user = User.new(email: "abc@gmail.com", password: 'x'*9, name: "x"*8)
        
        expect(user.timezone).to eq "Eastern Time (US & Canada)" # Verify
        
        user.timezone = nil
        expect(user).not_to be_valid
        user.timezone = ""
        expect(user).not_to be_valid
    end
    
    scenario "timezone can only be certain values" do
        # Create a user
        user = User.new(email: "abc@gmail.com", password: 'x'*9, name: "x"*8)
        
        expect(user.timezone).to eq "Eastern Time (US & Canada)" # Verify
        
        # SHall only belong to the US timezones... for now.
        # ["Hawaii", "Alaska", "Pacific Time (US & Canada)", "Arizona", "Mountain Time (US & Canada)", "Central Time (US & Canada)", "Eastern Time (US & Canada)", "Indiana (East)"]
        expect(user).to be_valid
        user.timezone = "Hawaii"
        expect(user).to be_valid
        user.timezone = "Alaska"
        expect(user).to be_valid
        user.timezone = "Pacific Time (US & Canada)"
        expect(user).to be_valid
        user.timezone = "Arizona"
        expect(user).to be_valid
        user.timezone = "Mountain Time (US & Canada)"
        expect(user).to be_valid
        user.timezone = "Central Time (US & Canada)"
        expect(user).to be_valid
        user.timezone = "Indiana (East)"
        expect(user).to be_valid
        
        # Don't accept anything else
        user.timezone = "blah"
        expect(user).not_to be_valid
    end
    
end
