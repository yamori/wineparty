# == Schema Information
#
# Table name: parties
#
#  id                :integer          not null, primary key
#  name              :string
#  event_date        :datetime
#  event_key         :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  user_id           :integer
#  concluded         :boolean
#  results_released  :boolean
#  disguise_theme_id :integer
#

require 'rails_helper'
require 'helpers'
include SpecHelper

RSpec.describe Party, :type => :model do

	describe "Party model" do
		let(:sampleParty) { FactoryGirl.create(:party) }
		it "should require name" do
			sampleParty.name = ""
			expect(sampleParty).not_to be_valid
		end

		it "should require date" do
			sampleParty.event_date = ""
			expect(sampleParty).not_to be_valid
		end
		it "should require user_id" do
			sampleParty.user_id = nil
			expect(sampleParty).not_to be_valid
		end
		it "should not be concluded (when first created)" do
			expect(sampleParty.concluded).to eq false
		end
		it "should not be in results_released state (when first created)" do
			expect(sampleParty.results_released).to eq false
		end

		describe "unique event_key" do
			let(:secondParty) { sampleParty.dup }
			it "should be invalid if repeated" do
				expect(secondParty).not_to be_valid
			end
		end
	end
	
	scenario "must have a disguise_theme_id" do 
		party = FactoryGirl.build(:party, disguise_theme_id: nil)
		expect(party).not_to be_valid
	end
	
	describe "Party and flipping the concluded status" do
		
		before(:all) do
			seed_disguise_table()
		end
		
		let!(:user) { FactoryGirl.create(:user) }
		let!(:user2) { FactoryGirl.create(:user) }
		let(:sampleParty) { FactoryGirl.create(:party, :user_id => user.id, disguise_theme_id: DisguiseTheme.first.id) }
		
		it "(verify concluded state)" do
			expect(sampleParty.concluded).to eq false
		end
		it "(verify no entries nor votes)" do
			expect(sampleParty.votes.size).to eq 0
			expect(sampleParty.entries.size).to eq 0
		end
		
		describe "using the flip_concluded method" do
			before do
				@message0 = sampleParty.flip_concluded
			end
			it "should not change the concluded status because there are no entries nor votes" do
				expect(sampleParty.concluded).to eq false
				expect(@message0).to eq "Party is still in progress.  Cannot close party if no one has voted"
			end
			
			describe "adding entries and votes" do
				before do
					PartyMembership.create(party_id: sampleParty.id, user_id: user.id)
					PartyMembership.create(party_id: sampleParty.id, user_id: user2.id)
					entry1 = Entry.create(user_id: user2.id, party_id: sampleParty.id, clearname: 'wineBottle')
					FactoryGirl.create(:vote, :user_id => user2.id, :party_id => sampleParty.id, :entry_id => entry1.id, :note => nil )
				end
				
				it "(verify the entry and vote)" do
					# Reload for some reason
					sampleParty = Party.first
					
					expect(sampleParty.entries.size).to eq 1
					expect(sampleParty.votes.size).to eq 1
				end
				
				describe "using the flip_conclude method" do
					before do
						# Reload for some reason
						sampleParty = Party.first
						@message = sampleParty.flip_concluded
					end
					
					it "should have flipped the status to concluded = true" do
						# Reload for some reason
						sampleParty = Party.first
						
						expect(sampleParty.concluded).to eq true
						expect(@message).to eq "Party is now concluded"
					end
					
					describe "flip the party back" do
						before do
							# Reload for some reason
							sampleParty = Party.first
							@message2 = sampleParty.flip_concluded
						end
						it "should fip the status to concluded = false" do
							# Reload for some reason
							sampleParty = Party.first
							
							expect(sampleParty.concluded).to eq false
							expect(@message2).to eq "Party is now in progress"
						end
					end
				end
			end
		end
	end
	

end
