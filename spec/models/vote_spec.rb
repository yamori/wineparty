# == Schema Information
#
# Table name: votes
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  party_id   :integer
#  entry_id   :integer
#  score      :integer
#  note       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'
require 'helpers'
include SpecHelper

RSpec.describe Vote, :type => :model do
  
	describe "Vote model" do
		
		before(:all) do
			seed_disguise_table()
		end

		let!(:user_host) { FactoryGirl.create(:user) }
		let!(:user) { FactoryGirl.create(:user) }
		let!(:sampleParty) { FactoryGirl.create(:party, :user_id => user_host.id, disguise_theme_id: DisguiseTheme.first.id) }
		let!(:entry) { FactoryGirl.create(:entry, :user_id => user_host.id, :party_id => sampleParty.id) }
		# Build but don't save to test validity
		let!(:vote) {FactoryGirl.build(:vote, :user_id => user.id, :party_id => sampleParty.id, 
			:entry_id => entry.id ) }

		it "should allow nil score" do
			vote.score = nil
			expect(vote).to be_valid
		end

		it "should require user_id" do
			vote.user_id = nil
			expect(vote).not_to be_valid
		end

		it "should require party_id" do
			vote.party_id = nil
			expect(vote).not_to be_valid
		end

		it "should require entry_id" do
			vote.entry_id = nil
			expect(vote).not_to be_valid
		end

		it "should require score >= 0" do
			vote.score = -1
			expect(vote).not_to be_valid
		end

		it "should require score <= 10" do
			vote.score = 11
			expect(vote).not_to be_valid
		end

		describe "a valid Vote instance" do
			before do
				vote.score = 10
				vote.save
			end
			it "should create a Vote record" do
				expect(Vote.all.size).to eq 1
			end

			describe "User association" do
				before do
					user.destroy
				end
				it "should also destroy vote" do
					expect(Vote.all.size).to eq 0
				end
			end

			describe "Party association" do
				before do
					sampleParty.destroy
				end
				it "should also destroy vote" do
					expect(Vote.all.size).to eq 0
				end
			end

			describe "Entry association" do
				before do
					entry.destroy
				end
				it "should also destroy vote" do
					expect(Vote.all.size).to eq 0
				end
			end

			describe "duplicate Vote" do
				before do
					@duplicateVote = vote.dup
				end
				it "should not be valid" do
					expect(@duplicateVote).not_to be_valid
				end
			end
		end

		describe "a host trying to vote in own Party" do
			# Build but don't save to test validity
			let!(:vote_ownParty) {FactoryGirl.build(:vote, :user_id => user_host.id, :party_id => sampleParty.id, 
			:entry_id => entry.id ) }

			it "should not be valid" do
				expect(vote_ownParty).not_to be_valid
			end

			describe "trying to save same vote in own Party" do

				before do
					vote_ownParty.save
				end
				it "should not persist" do
					expect(Vote.all.size).to eq 0
				end
			end
			
		end

	end
	
	describe "Vote model member methods" do
		
		scenario "has method to return a random vote" do
			
			seed_disguise_table()
			
			user_host = FactoryGirl.create(:user)
			sampleParty = FactoryGirl.create(:party, :user_id => user_host.id, disguise_theme_id: DisguiseTheme.first.id)
			# First user.
			user1 = FactoryGirl.create(:user)
			entry1 = FactoryGirl.create(:entry, :user_id => user1.id, :party_id => sampleParty.id)
			
			# vote.note is nil
			vote1 = FactoryGirl.create(:vote, :user_id => user1.id, :party_id => sampleParty.id, :entry_id => entry1.id, :note => nil )
			# the method should not return it.
			expect(Vote.randomVoteWithNote(sampleParty.id)['vote_with_note']).to eq nil
			expect(Vote.randomVoteWithNote(sampleParty.id)['number_votes_with_note']).to eq 0
			
			# vote.note is not nil, but now empty
			vote1.update(:note => "")
			# the method should not return it.
			expect(Vote.randomVoteWithNote(sampleParty.id)['vote_with_note']).to eq nil
			expect(Vote.randomVoteWithNote(sampleParty.id)['number_votes_with_note']).to eq 0
			
			# vote.note has non-empty content (valid)
			vote1.update(:note => "non-empty")
			# the method should not return it.
			expect(Vote.randomVoteWithNote(sampleParty.id)['vote_with_note']).not_to eq nil
			expect(Vote.randomVoteWithNote(sampleParty.id)['number_votes_with_note']).to eq 1
		end
		
	end
end
