# == Schema Information
#
# Table name: shouts
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  party_id   :integer
#  content    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Shout, :type => :model do
    
    scenario "must have user id" do
        
        # Create a party
        user_host = FactoryGirl.create(:user)
        user = FactoryGirl.create(:user)
        sampleParty = FactoryGirl.create(:party, :user_id => user_host.id)
        
        # Try to create shout
        shout = Shout.new(content: "hi", party_id: sampleParty.id)
        expect(shout).not_to be_valid
        
    end
    
    scenario "must have party id" do
        
        # Create a party
        user_host = FactoryGirl.create(:user)
        user = FactoryGirl.create(:user)
        sampleParty = FactoryGirl.create(:party, :user_id => user_host.id)
        
        # Try to create shout
        shout = user.shouts.new(content: "hi")
        expect(shout).not_to be_valid
        
    end
    
    scenario "must have content" do
        
        # Create a party
        user_host = FactoryGirl.create(:user)
        user = FactoryGirl.create(:user)
        sampleParty = FactoryGirl.create(:party, :user_id => user_host.id)
        
        # Try to create shout - no content
        shout = user.shouts.new(party_id: sampleParty.id)
        expect(shout).not_to be_valid
        
        # Try to create shout - zero length content
        shout2 = user.shouts.new(party_id: sampleParty.id, content: "")
        expect(shout2).not_to be_valid
        
    end
    
    scenario "User has many Shouts" do
        
        # Create a party
        user_host = FactoryGirl.create(:user)
        user = FactoryGirl.create(:user)
        sampleParty = FactoryGirl.create(:party, :user_id => user_host.id)
        
        # Create some shouts
        shout1 = user.shouts.create(party_id: sampleParty.id, content: "hi")
        shout2 = user.shouts.create(party_id: sampleParty.id, content: "yo")
        expect(Shout.all.size).to eq 2
        
    end
    
    scenario "User dependent for destroy" do
        
        # Create a party
        user_host = FactoryGirl.create(:user)
        user = FactoryGirl.create(:user)
        sampleParty = FactoryGirl.create(:party, :user_id => user_host.id)
        
        # Create some shouts
        shout1 = user.shouts.create(party_id: sampleParty.id, content: "hi")
        shout2 = user.shouts.create(party_id: sampleParty.id, content: "yo")
        expect(Shout.all.size).to eq 2
        
        # Destroy
        user.destroy
        expect(Shout.all.size).to eq 0
    end
    
    scenario "Party has many Shouts" do
        
        # Create a party
        user_host = FactoryGirl.create(:user)
        user = FactoryGirl.create(:user)
        sampleParty = FactoryGirl.create(:party, :user_id => user_host.id)
        
        # Create some shouts
        user.shouts.create(party_id: sampleParty.id, content: "hi")
        user.shouts.create(party_id: sampleParty.id, content: "yo")
        user_host.shouts.create(party_id: sampleParty.id, content: "oh")
        
        # Verify
        expect(sampleParty.shouts.size).to eq 3
    end
    
    scenario "Party dependent for destroy" do
        
        # Create a party
        user_host = FactoryGirl.create(:user)
        user = FactoryGirl.create(:user)
        sampleParty = FactoryGirl.create(:party, :user_id => user_host.id)
        
        # Create some shouts
        user.shouts.create(party_id: sampleParty.id, content: "hi")
        user.shouts.create(party_id: sampleParty.id, content: "yo")
        user_host.shouts.create(party_id: sampleParty.id, content: "oh")
        
        # Verify
        expect(sampleParty.shouts.size).to eq 3
        
        # Destroy
        sampleParty.destroy
        expect(sampleParty.shouts.size).to eq 0
    end
    
end
