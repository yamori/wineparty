# == Schema Information
#
# Table name: entries
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  party_id    :integer
#  clearname   :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  wine_id     :integer
#  disguise_id :integer
#

require 'rails_helper'
require 'helpers'
include SpecHelper

RSpec.describe Entry, :type => :model do

	describe "Entry model" do
		
		before(:all) do
			seed_disguise_table()
		end

		let!(:user) { FactoryGirl.create(:user) }
		let!(:sampleParty) { FactoryGirl.create(:party, :user_id => user.id, disguise_theme_id: DisguiseTheme.first.id) }

		describe "creating an Entry with missing fields" do

			before do
				user.entries.create 			#missing party_id
				sampleParty.entries.create 		#missing user_id
				user.entries.create(party_id: sampleParty.id)	#missing clearname:
			end

			it "should not create a record" do
				expect(Entry.all.size).to   eq 0
			end
		end

		describe "an Entry associated to a User and Party" do

			before do
				FactoryGirl.create(:entry, :user_id => user.id, :party_id => sampleParty.id)
			end
			it "should exist" do
				expect(Entry.all.size).to   eq 1
			end
			it "should be associated to a disguise" do
				expect(user.entries.first.disguise_id.nil?).to eq false
			end

			describe "when clearname is not present" do
				before do
					@entry = user.entries.first
					@entry.clearname = ""
				end
				it "should not be valid" do
					expect(@entry).not_to be_valid
				end
			end

			describe "when the User is destroyed" do
				before do
					user.destroy
				end
				it "should destroy associated Entry" do
					expect(Entry.all.size).to   eq 0
				end
			end

			describe "when the Party is destroyed" do
				before do
					sampleParty.destroy
				end
				it "should destroy associated Entry" do
					expect(Entry.all.size).to   eq 0
				end
			end
		end

		describe "an Entry saved with a never before seen wine" do
			before do
				@entry = FactoryGirl.create(:entry, :user_id => user.id, :party_id => sampleParty.id)
			end
			it "should create a corresponding Wine record" do
				expect(Wine.all.size).to   eq 1
			end
			it "should be associated to the new Wine reocrd" do
				expect(@entry.wine.id).to   eq Wine.first.id
			end
			it "should have clearname be the same as the new Wine record" do
				expect(@entry.clearname).to   eq Wine.first.name
			end

			describe "updating the Entry's clearname" do
				before do
					@entry.update(clearname: @entry.clearname + "different")
				end
				it "should create a corresponding Wine record" do
					expect(Wine.all.size).to   eq 2
				end
				it "should be associated to the new Wine reocrd" do
					expect(@entry.wine_id).to   eq Wine.last.id
				end
				it "should have clearname be the same as the new Wine record" do
					expect(@entry.clearname).to   eq Wine.last.name
				end

				describe "a second entry with a previous Wine name" do
					before do
						user2 = FactoryGirl.create(:user)
						@entry2 = FactoryGirl.create(:entry, :user_id => user2.id, :party_id => sampleParty.id, :clearname => Wine.first.name)
					end
					it "should match the previous Wine record" do
						expect(@entry2.wine_id).to eq Wine.first.id
						expect(Wine.all.size).to eq 2 #unchanged
					end
				end
			end

			describe "destroying the Entry" do
				before do
					@wine_id = @entry.wine_id
					@entry.destroy
				end
				it "should not destroy the original Wine record" do
					expect(Wine.exists?(@wine_id)).to eq true
				end
			end
		end
	end
	
	describe "Entry.party_members_accepted_invite_no_entry" do
		
		before(:all) do
			seed_disguise_table()
		end
		
		let!(:user) { FactoryGirl.create(:user) }
		let!(:sampleParty) { FactoryGirl.create(:party, :user_id => user.id, disguise_theme_id: DisguiseTheme.first.id) }

		it "should return hosts email when host hasn't registed a wine" do
			expect( Entry.party_members_accepted_invite_no_entry(sampleParty.id) ).to  eq [user.email]
		end
		
		describe "second user is part of party" do
			let!(:user2) { FactoryGirl.create(:user) }
			before do
				PartyMembership.create(party_id: sampleParty.id, user_id: user2.id)
			end
			
			it "(should reflect two party members)" do
				expect( sampleParty.party_memberships.size).to eq 2
			end
			it "party_members_accepted_invite_no_entry should return two email" do
				emails = Entry.party_members_accepted_invite_no_entry(sampleParty.id)
				expect( emails.include? user.email ).to eq true
				expect( emails.include? user2.email ).to eq true
			end
			
			describe "when first user registers their wine" do
				before do
					Entry.create(user_id: user.id, party_id: sampleParty.id, clearname: 'wineBottle')
				end
				it "party_members_accepted_invite_no_entry should not have user's email" do
					emails = Entry.party_members_accepted_invite_no_entry(sampleParty.id)
					expect( emails.include? user.email ).to eq false
				end
				it "party_members_accepted_invite_no_entry should have user2's email" do
					emails = Entry.party_members_accepted_invite_no_entry(sampleParty.id)
					expect( emails.include? user2.email ).to eq true
				end
				
				describe "when second user registers their wine" do
					before do
						Entry.create(user_id: user2.id, party_id: sampleParty.id, clearname: 'wineBottle')
					end
					it "party_members_accepted_invite_no_entry should be nil" do
						expect( Entry.party_members_accepted_invite_no_entry(sampleParty.id) ).to  eq nil
					end
				end
			end
		end
		
	end
end
