require 'rails_helper'

RSpec.describe PartyController, :type => :controller do

	def valid_session
	  {"warden.user.user.key" => session["warden.user.user.key"]}
	end


	describe "wrong user accessing patry controller" do

		let(:firstUser) { FactoryGirl.create(:user) }
		let(:firstUsersParty) { FactoryGirl.create(:party,:user_id => firstUser.id) }
		let(:secondUser) { FactoryGirl.create(:user) }

		it "(confirm first users Party)" do
			expect(Party.where(name:firstUsersParty.name)).to exist
		end

		describe "attempting to access party#create" do
			it "should not allow access" do
				sign_in secondUser
				put :update, {party: { name: firstUsersParty.name, event_date: firstUsersParty.event_date }, id: firstUsersParty.id}, valid_session

				expect(flash[:error]).to match(/^Something went wrong/)
			end
		end

		describe "attempting to access party#delete" do
			it "should not allow access" do
				sign_in secondUser
				delete :destroy, { party: { id: firstUsersParty.id }, id: firstUsersParty.id }, valid_session

				expect(flash[:error]).to match(/^Something went wrong/)
			end
		end

	end

end
