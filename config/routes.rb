Rails.application.routes.draw do

  devise_for :users, :controllers => { :omniauth_callbacks => "callbacks" }
  
  # Party
  resources :party,  only: [:show, :new, :create, :edit, :update, :destroy]
  put 'party/:id/conclude_flip' => 'party#conclude_flip', as: :party_conclude_flip
  put 'party/:id/results_released_flip' => 'party#results_released_flip', as: :results_released_flip
  get 'party/:id/view_results' => 'party#view_results', as: :party_view_results
  get 'party/state_check/:id' => 'party#state_check', as: :party_state_check
  get 'party/:id/get_status' => 'party#get_status', as: :party_get_status

  # PartyInvitation
  resources :party_invitation, only: [:create]
  put 'party_invitation/:id/accept' => 'party_invitation#accept', as: :party_invitation_accept

  # Entry
  put 'entry_upsert_clearname/:entry_id' => 'entry#upsert_clearname', as: :entry_user_upsert_clearname

  #Vote
  resources :vote, only: [:update]
  get 'vote/:id/dashboard_ajax' => 'vote#dashboard_ajax', as: :vote_dashboard_ajax

  #Wine
  get 'wine/ajax_search'
  
  # Shout
  put 'shout/create'
  get 'shout/:id/dashboard_ajax' => 'shout#dashboard_ajax', as: :shout_dashboard_ajax
  
  # User management - 'user' instead of 'users' to not conflict with devise
  get 'user/:id/edit' => 'user#edit', as: :user_edit
  put 'user/:id' => 'user#update', as: :user_update
  put 'user/update/:user_id' => 'user#update_nickname', as: :update_user_nickname
  delete 'user/:id' => 'user#destroy', as: :user_destroy

  root :to => "static#index"

  get '/signout' => 'sessions#destroy'
  #match "/signout" => "sessions#destroy", :as => :signout

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
