# == Schema Information
#
# Table name: disguises
#
#  id                :integer          not null, primary key
#  name              :string
#  disguise_theme_id :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Disguise < ActiveRecord::Base
    validates :name, presence: true
    validates :name, length: { minimum: 3 }
    validates :name, uniqueness: { scope: :disguise_theme_id, message: "disguise must be unique within theme", case_sensitive: false }
    validates :disguise_theme_id, presence: true
    
    # Associations
    belongs_to :disguise_theme
    has_many :entries
end
