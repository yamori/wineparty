# == Schema Information
#
# Table name: party_memberships
#
#  id         :integer          not null, primary key
#  party_id   :integer
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class PartyMembership < ActiveRecord::Base
	validates :party_id, :user_id, presence: true

	#Association
	belongs_to :user
	belongs_to :party
end
