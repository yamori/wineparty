# == Schema Information
#
# Table name: votes
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  party_id   :integer
#  entry_id   :integer
#  score      :integer
#  note       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Vote < ActiveRecord::Base
	
	#Validations
	validates :party_id, :user_id, :entry_id, presence: true
	validates_uniqueness_of :entry_id, :scope => [:user_id, :party_id]
	validates_numericality_of :score, :only_integer => true, :allow_nil => true, 
	    :greater_than_or_equal_to => 0,
	    :less_than_or_equal_to => 10,
	    :message => "can only be integer between 0 and 10 inclusive."
	validate :host_cannot_vote_at_own_party

	#Associations
	belongs_to :user
	belongs_to :party
	belongs_to :entry

	def host_cannot_vote_at_own_party
		if !party_id.nil? && !user_id.nil? && user_id==self.party.user_id
			errors.add(:user_id, "host of party cannot vote at own party")
		end
	end
	
	def self.randomVoteWithNote(party_id)
		# Given a party_id, return a random vote wtih a non-empty note
		votes_with_note = Party.find(party_id).votes.where("note IS NOT NULL").where.not(note: '')
		# Return a hash
		return {'vote_with_note' => votes_with_note.sample, 'number_votes_with_note' => votes_with_note.size}
	end

	def self.retrieve_votes(party_id, user_id)
		# Given a party_id, fetch existing votes.  If existing votes don't cover
		#  all entries eligible for votes, persist new empty votes

		eligible_entries = Entry.where(party_id: party_id).where.not(disguise_id: nil).pluck(:id)

		existing_votes = Vote.where(party_id: party_id, user_id: user_id, entry_id: eligible_entries)

		entry_ids_needing_shell_votes = eligible_entries - existing_votes.pluck(:entry_id)

		vote_array = [] #Mix of existing votes and shell votes
		existing_votes.each do |existing_vote|
			vote_array.push(existing_vote)
		end
		entry_ids_needing_shell_votes.each do |entry_id|
			shell_vote = Vote.create(party_id: party_id, user_id: user_id, entry_id: entry_id)
			vote_array.push(shell_vote)
		end

		# Alphabetize the votes by codename so party attendees will have easier time finding.
		vote_array_alphabetized = vote_array.sort_by { |vote| vote.entry.codename }
		
		return vote_array_alphabetized
	end

	def self.spectate_votes(party_id)
		party = Party.find(party_id)
		allScores = Vote.where(party_id: party_id).where.not(score:nil).pluck(:score)

		stats = Hash.new
		stats["current_number_votes"] = allScores.size
		max_number_votes = party.entries.size * (party.party_memberships.size - 1) # Subtract 1 because host can't vote
		stats["max_number_votes"] = max_number_votes
		stats["vote_completion"] = "#{((allScores.size.to_f / max_number_votes.to_f)*100).round.to_s}%"
		stats["mean"] = Statistics::Basicstats.mean(allScores).round(2)
		stats["sample_variance"] = Statistics::Basicstats.sample_variance(allScores).round(2)
		stats["histogram"] = Statistics::Basicstats.histogram((0..10).to_a,allScores)
		return stats
	end

	def self.present_results(party_id, current_user_id)
		#Return object
		results = Hash.new

		#Common queried data used for calculations.  DRY.
		hash_lookups = Entry.entry_hash_lookups(party_id)
		entries_eligible = 		hash_lookups["entries_eligible"]
		entry_id_to_codename = 	hash_lookups["entry_id_to_codename"]
		entry_id_to_clearname = hash_lookups["entry_id_to_clearname"]
		votes_not_nil_ordered = Vote.where(party_id: party_id).where.not(score: nil).order("updated_at ASC")
		# member_userID_email_hash = Hash[User.where(id: PartyMembership.where(party_id: party_id).pluck(:user_id)).pluck(:id,:email)]
		partyMembershipUserIDs = PartyMembership.where(party_id: party_id).pluck(:user_id)
		member_userID_email_hash = Hash[User.where(id: partyMembershipUserIDs).map{|user| [user.id, user.needs_name ? user.email : user.name]} ]
		user_id_to_entry_id = Hash[Entry.where(party_id: party_id).pluck(:user_id,:id)]
		entry_id_to_user_id = []
		user_id_to_entry_id.each do |key,value|
			entry_id_to_user_id.push([value,key])
		end
		entry_id_to_user_id = Hash[entry_id_to_user_id]

		# First to vote
		if votes_not_nil_ordered.size>0
			first_to_vote = votes_not_nil_ordered.first
			results["first_to_vote"] = Hash.new
			results["first_to_vote"]["time"] = first_to_vote.updated_at
			results["first_to_vote"]["score"] = first_to_vote.score
			results["first_to_vote"]["name"] = member_userID_email_hash[first_to_vote.user_id]
			results["first_to_vote"]["codename"] = entry_id_to_codename[first_to_vote.entry_id]
		else
			results["first_to_vote"] = nil
		end

		#First to complete sampling
		results["first_to_complete"] = nil
		number_of_samples = entries_eligible.size
		completion_tracking_hash = Hash.new
		#Votes are ascending by updated_at, crawl through until someone has number_of_samples
		votes_not_nil_ordered.each do |vote|
			usr_id = vote.user_id.to_s
			if completion_tracking_hash[usr_id].nil?
				completion_tracking_hash[usr_id] = 1
			else
				completion_tracking_hash[usr_id] = completion_tracking_hash[usr_id] + 1
			end

			#Check if full sampling achieved
			if completion_tracking_hash[usr_id]==number_of_samples
				results["first_to_complete"] = Hash.new
				results["first_to_complete"]["time"] = vote.updated_at
				results["first_to_complete"]["name"] = member_userID_email_hash[vote.user_id]
				break
			end
		end

		#Highest and lowest vote for own entry
		results["highest_own_vote"] = nil
		results["lowest_own_vote"] = nil
		#Crawl through votes, discover own votes and keep track of highest
		highwater_mark = 0;
		lowwater_mark = 10;
		votes_not_nil_ordered.each do |vote|
			if vote.entry_id==user_id_to_entry_id[vote.user_id]
				#Own vote

				# High vote
				if results["highest_own_vote"].nil?
					results["highest_own_vote"] = []
				end
				if vote.score==highwater_mark
					#Push vote onto array
					own_vote_obj = Hash.new
					own_vote_obj["score"] = vote.score
					own_vote_obj["name"] = member_userID_email_hash[vote.user_id]
					own_vote_obj["codename"] = entry_id_to_codename[vote.entry_id]
					results["highest_own_vote"].push( own_vote_obj )
				elsif 	vote.score>highwater_mark
					#Clear the previous high
					results["highest_own_vote"] = []

					#Push vote onto array
					own_vote_obj = Hash.new
					own_vote_obj["score"] = vote.score
					own_vote_obj["name"] = member_userID_email_hash[vote.user_id]
					own_vote_obj["codename"] = entry_id_to_codename[vote.entry_id]
					results["highest_own_vote"].push( own_vote_obj )

					highwater_mark = vote.score
				end

				# Low Vote
				if results["lowest_own_vote"].nil?
					results["lowest_own_vote"] = []
				end
				if vote.score==lowwater_mark
					#Push vote onto array
					own_vote_obj = Hash.new
					own_vote_obj["score"] = vote.score
					own_vote_obj["name"] = member_userID_email_hash[vote.user_id]
					own_vote_obj["codename"] = entry_id_to_codename[vote.entry_id]
					results["lowest_own_vote"].push( own_vote_obj )
				elsif 	vote.score<lowwater_mark
					#Clear the previous low
					results["lowest_own_vote"] = []

					#Push vote onto array
					own_vote_obj = Hash.new
					own_vote_obj["score"] = vote.score
					own_vote_obj["name"] = member_userID_email_hash[vote.user_id]
					own_vote_obj["codename"] = entry_id_to_codename[vote.entry_id]
					results["lowest_own_vote"].push( own_vote_obj )

					lowwater_mark = vote.score
				end
			end
		end

		# Rank Statistics
		calculate_scores_obj = calculate_scores(party_id)
		entry_scores = calculate_scores_obj["entry_scores"]
		average_scores = calculate_scores_obj["average_scores"]

		#Top three
		results["top_three"] = Hash.new
		results["top_three"]["first"] = nil
		results["top_three"]["second"] = nil
		results["top_three"]["third"] = nil

		average_scores_sorted = average_scores.sort_by {|entryID,score| -score } # High to low
		position_arr = ["first","second","third"]
		position_indx = -1
		moving_score = 11.0
		average_scores_sorted.each do |entryID,score|

			userObj = Hash.new
			userObj["score"] = score
			userObj["codename"] = entry_id_to_codename[entryID]
			userObj["name"] = member_userID_email_hash[entry_id_to_user_id[entryID]]
			userObj["clearname"] = entry_id_to_clearname[entryID]

			if score>=moving_score
				# Insert at current level (in case there are ties)
				results["top_three"][position_arr[position_indx]] = userObj
			else
				position_indx = position_indx + 1
				break if position_indx>=position_arr.size # Finished filling out results["top_three"]
				
				results["top_three"][position_arr[position_indx]] = userObj
			end

		end

		# Bottom three
		average_scores_sorted_low2high = average_scores_sorted.reverse # Low to high
		if average_scores.size<6
			#Don't do stats for bottom if < 6
			results["bottom_three"] = nil
		else
			results["bottom_three"] = Hash.new
			results["bottom_three"]["third_last"] = nil
			results["bottom_three"]["second_last"] = nil
			results["bottom_three"]["last"] = nil

			average_scores_sorted = average_scores_sorted.reverse # Low to high
			position_arr = ["last","second_last","third_last"]
			position_indx = -1
			moving_score = -1.0
			average_scores_sorted_low2high.each do |entryID,score|

				userObj = Hash.new
				userObj["score"] = score
				userObj["codename"] = entry_id_to_codename[entryID]
				userObj["name"] = member_userID_email_hash[entry_id_to_user_id[entryID]]
				userObj["clearname"] = entry_id_to_clearname[entryID]

				if score<=moving_score
					# Insert at current level (in case there are ties)
					results["bottom_three"][position_arr[position_indx]] = userObj
				else
					position_indx = position_indx + 1
					break if position_indx>=position_arr.size # Finished filling out results["bottom_three"]
					
					results["bottom_three"][position_arr[position_indx]] = userObj
				end
			end
		end

		# Final Averages
		results["final_averages"] = Hash.new
		results["final_averages"]["averages"] = []
		results["final_averages"]["codenames"] = []
		results["final_averages"]["clearnames"] = []
		results["final_averages"]["name"] = []
		results["final_averages"]["own_vote"] = []
		results["final_averages"]["own_note"] = []
		average_scores_sorted_low2high.each do |entryID,score|
			results["final_averages"]["averages"].push(score)

			codename = entry_id_to_codename[entryID]
			results["final_averages"]["codenames"].push(codename)

			clearname = entry_id_to_clearname[entryID]
			results["final_averages"]["clearnames"].push(clearname)

			email = member_userID_email_hash[entry_id_to_user_id[entryID]]
			results["final_averages"]["name"].push(email)

			own_vote = votes_not_nil_ordered.where(user_id: current_user_id).where(entry_id: entryID)
			results["final_averages"]["own_vote"].push( own_vote.size==1 ? own_vote.first.score : "(no vote)" )
			results["final_averages"]["own_note"].push( own_vote.size==1 ? own_vote.first.note : "(no note)" )
		end

		
		return results
	end


	def self.voting_breakdown(party_id, user_id)
		# Creates a Hash obj containing voting histogram for each entry.

		#Return object
		breakdown = Hash.new

		# Rank Statistics
		calculate_scores_obj = calculate_scores(party_id, user_id)
		binned_scores_user_excluded = calculate_scores_obj["binned_scores_user_excluded"]
		binned_scores_only_user = calculate_scores_obj["binned_scores_only_user"]
		entry_scores = calculate_scores_obj["entry_scores"]
		average_scores = calculate_scores_obj["average_scores"]

		# Supporting Hashes
		hash_lookups = Entry.entry_hash_lookups(party_id)
		entries_eligible = 		hash_lookups["entries_eligible"]
		entry_id_to_codename = 	hash_lookups["entry_id_to_codename"]
		entry_id_to_clearname = hash_lookups["entry_id_to_clearname"]

		average_scores.each do |entry_id,avg_score|
			entry_obj = Hash.new
			entry_obj["binned_scores_user_excluded"] = binned_scores_user_excluded[entry_id]
			entry_obj["binned_scores_only_user"] = binned_scores_only_user[entry_id]
			entry_obj["entry_scores"] = entry_scores[entry_id]
			entry_obj["average_score"] = average_scores[entry_id]

			entry_obj["codename"] = entry_id_to_codename[entry_id]
			entry_obj["clearname"] = entry_id_to_clearname[entry_id]

			breakdown[entry_id] = entry_obj
		end

		#Sorty by average score
		breakdown = breakdown.sort_by{|key,value| -breakdown[key]["average_score"] }
		return breakdown

	end


	def self.calculate_scores(party_id, user_id = -1)
		# This does the heavy lifting for average score computation.  Three structs (hashed by entry_id):
		# binned_scores*: Histogram of scoring
		#   _all: includes all votes
		#   _user_excluded: all but votes belonging to user_id
		#   _only_user: only for user_id
		# entry_scores: Aggregated scores
		# average_scores: Average score

		# Ideally the math would be DB side but can't figure out ActiveRecord quirky grouping issues.

		#return object
		calculate_scores_obj = Hash.new

		# Data objects
		binned_scores_all = Hash.new
		binned_scores_user_excluded = Hash.new
		binned_scores_only_user = Hash.new
		entry_scores = Hash.new
		average_scores = Hash.new

		entries_eligible = Entry.where(party_id: party_id).where.not(disguise_id: nil)
		entries_eligible.each do |entry| 
			# Important to avoid NaN division
			next if entry.votes.where.not(score: nil).size==0
			
			binned_scores_all[entry.id] = Array.new(11,0) #Empty arrays to indicate number of votes
			binned_scores_user_excluded[entry.id] = user_id==-1 ? nil : Array.new(11,0) #Only if user_id argument supplied.
			binned_scores_only_user[entry.id] = user_id==-1 ? nil : Array.new(11,0) #Only if user_id argument supplied.
			entry_scores[entry.id] = [] #Collect votes scores per entry.
			average_scores[entry.id] = 0 #Initialize before averaging
		end

		#Bin votes by entry and score
		all_votes = Vote.where(party_id: party_id).where.not(score:nil).pluck(:entry_id,:score, :user_id);
		all_votes.each do |vote| 
			binned_scores_all[vote[0]][vote[1]] =  binned_scores_all[vote[0]][vote[1]] + 1
			if user_id!=-1 #Only if user_id argument supplied.
				if vote[2]==user_id
					binned_scores_only_user[vote[0]][vote[1]] =  binned_scores_only_user[vote[0]][vote[1]] + 1
				else
					binned_scores_user_excluded[vote[0]][vote[1]] =  binned_scores_user_excluded[vote[0]][vote[1]] + 1
				end
			end
			entry_scores[vote[0]].push( vote[1] )
		end

		#Compute average per entry
		entry_scores.each do |key,score_arr|
			average_scores[key] = Statistics::Basicstats.mean(score_arr).round(2)
		end

		#Build return object
		calculate_scores_obj["binned_scores_all"] = binned_scores_all
		calculate_scores_obj["binned_scores_user_excluded"] = binned_scores_user_excluded
		calculate_scores_obj["binned_scores_only_user"] = binned_scores_only_user
		calculate_scores_obj["entry_scores"] = entry_scores
		calculate_scores_obj["average_scores"] = average_scores
		return calculate_scores_obj

	end

	# Keep private but still class.
	private_class_method :calculate_scores

end
