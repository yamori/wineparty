# == Schema Information
#
# Table name: parties
#
#  id                :integer          not null, primary key
#  name              :string
#  event_date        :datetime
#  event_key         :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  user_id           :integer
#  concluded         :boolean
#  results_released  :boolean
#  disguise_theme_id :integer
#

class Party < ActiveRecord::Base
	validates :name, :user_id, presence: true
	validates :event_key, presence: true, uniqueness: { case_sensitive: false }
	validate :valid_datetime?
	validates :name, presence: true
	validates :disguise_theme_id, presence: true

	after_initialize :set_new_record_values
	after_create :create_owners_membership
	after_update :check_disguise_theme_update

	# Associations
	belongs_to :user
	has_many :party_invitations, dependent: :destroy
	has_many :party_memberships, dependent: :destroy
	has_many :entries, dependent: :destroy
	has_many :votes, dependent: :destroy
	has_many :shouts, dependent: :destroy

	def valid_datetime?
		begin
			DateTime.parse(self.event_date.to_s)
		rescue => error
			errors.add(:event_date, "is missing or invalid")
		end
	end

	def event_key_generator()
		valid_chars = "ABCDEFGHJKLMNPQRSTUVWXYZ123456789" #omitting ambiguous chars
		new_key = ""
		7.times do
			new_key << valid_chars[rand(valid_chars.length)]
		end
		return new_key
	end
	
	def getMembersAvgScores
		memberScoresArr = []
		partyAttendee_ids = party_memberships.pluck(:user_id)
		User.find(partyAttendee_ids).each do |attendeeUser|
			
			scores = attendeeUser.votes(party_id: self.id).pluck(:score)
			
			nonNil_scores = scores.compact
			average = nonNil_scores.sum / nonNil_scores.size.to_f
			if average.nan?
				next
			end
			
			newHash = {"email"=> attendeeUser.email, "average_score" => average }
			
			memberScoresArr.push(newHash)
		end
		
		# Sort in descending score order
		memberScoresArr.sort_by! { |item| -1*item["average_score"] }
		
		return memberScoresArr
	end
	
	def flip_concluded
		# Contains logic for flipping party state
		
		# Allow concluded: false-->true only if there exist entries & votes
		if !self.concluded && self.entries.size>0 && self.votes.size>0
			self.concluded = true
			self.results_released = false
			self.save
			return "Party is now concluded"
		elsif !self.concluded
			# Party is !concluded, and will stay !concluded because entries and votes are needed
			return "Party is still in progress.  Cannot close party if no one has voted"
		else
			# Party is concluded, toggle back to !concluded
			self.concluded = false
			self.results_released = false
			self.save
			return "Party is now in progress"
		end
	end

	private

		def set_new_record_values
			if self.new_record? #Only for new record
				begin
					self.event_key = event_key_generator()
				end while (not Party.find_by_event_key(self.event_key).blank?)

				self.concluded = false
				self.results_released = false
			end
			true
		end

		def create_owners_membership
			# The party owner must be a member to their own party
			PartyMembership.create(party_id: self.id, user_id: self.user_id)
		end
		
		def check_disguise_theme_update
			# IF the disguise_theme has been updated, all Party's entries need to get new Disguises
			if self.disguise_theme_id_changed?
				self.entries.each do |partyEntry|
					partyEntry.find_new_disguise_id
				end
			end
		end
		
end
