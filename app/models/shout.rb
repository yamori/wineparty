# == Schema Information
#
# Table name: shouts
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  party_id   :integer
#  content    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Shout < ActiveRecord::Base
    
    #Validations
	validates :party_id, :user_id, :content, presence: true
	validates :content, length: { minimum: 1 }
    
    #Associations
	belongs_to :user
	belongs_to :party
	
end
