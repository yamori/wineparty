# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default("0"), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime
#  updated_at             :datetime
#  provider               :string
#  uid                    :string
#  name                   :string
#  timezone               :string
#

class User < ActiveRecord::Base
	#Associations
	has_many :parties, dependent: :destroy
	has_many :party_invitations, dependent: :destroy
	has_many :party_memberships, dependent: :destroy
	has_many :entries, dependent: :destroy
	has_many :votes, dependent: :destroy
	has_many :shouts, dependent: :destroy
	
	# Validations
	validate :validate_name_length
	validate :validate_timezone

	#Callbacks
	after_create :gather_invites
	after_initialize :default_timezone
	
	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, :async, :registerable,
	     :recoverable, :rememberable, :trackable, :validatable,
	     :omniauthable, :omniauth_providers => [:facebook]


	def accept_invite(party_id)
		# Using the existing Invite, create a membership record and delete the invite.
		result = true #Assume true
		existing_invite = PartyInvitation.where(party_id: party_id, user_id: self.id).first
		new_membership = PartyMembership.new(party_id: existing_invite.party_id, user_id: self.id)
		if new_membership.save
			#Only destroy the invite if the PartyMembership creation is success.
			existing_invite.destroy
		else
			result = false
		end
		return result
	end

	def eager_get_invitation_details
		#Used for view
		PartyInvitation.includes(:party).where(user_id: self.id).sort { |a,b| a.party.event_date <=> b.party.event_date }
	end

	def get_upcoming_parties
		all_parties = PartyMembership.where(user_id: self.id).pluck(:party_id)
		upcoming_parties = Party.where("id IN (?) AND event_date >= ?", all_parties, (Date.today-1.day).to_datetime ).order(event_date: :asc)
		return upcoming_parties
	end

	def get_previous_parties
		all_parties = PartyMembership.where(user_id: self.id).pluck(:party_id)
		previous_parties = Party.where("id IN (?) AND event_date < ?", all_parties, (Date.today-1.day).to_datetime ).order(event_date: :desc)
		return previous_parties
	end

	def find_or_build_entry(party_id)
		self.entries.where(party_id: party_id).first || self.entries.new(party_id: party_id)
	end


	def self.from_omniauth(auth)
		where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
			user.provider = auth.provider
			user.uid = auth.uid
			user.email = auth.info.email
			user.password = Devise.friendly_token[0,20]
		end
	end
	
	def needs_name
		# Return T or F depending on same validtion from validate_name_length
		if (self.name.nil? || self.name.length<6)
			return true
		else
			return false
		end
	end
	
	def validate_name_length
		# If name is not nil or empty, ensure it is >=6 chars
		if (!self.name.nil? && self.name.length>0)
			if (self.name.length<6)
				errors.add(:name_length, "name needs to be more 6 or more characters in length")
			end
		end
	end
	
	def self.recognizedTimeZones
		["Eastern Time (US & Canada)", "Indiana (East)",  "Central Time (US & Canada)", "Mountain Time (US & Canada)", "Arizona", "Pacific Time (US & Canada)", "Hawaii", "Alaska"]
	end
	
	def validate_timezone
		# Custom validate what timezone we're accepting
		if (self.timezone.nil? || self.timezone.empty?)
			errors.add(:timezone, "invalid timezone")
		end
		accepted_tzones = User.recognizedTimeZones
		if (!accepted_tzones.include?(self.timezone))
			errors.add(:timezone, "invalid timezone")
		end
		# Otherwise it's good.
	end

	private

		def gather_invites
			# When a user signs up, gather all PartyInvitations which have the users email
			#PartyInvitation.where(email: self.email).update_all(user_id: self.id)
			PartyInvitation.where('lower(email) = ?', self.email).update_all(user_id: self.id)
		end
		
		def default_timezone
			# Set the default TZone when a User is first created
			if (self.timezone.nil? || self.timezone.empty?)
				self.timezone = "Eastern Time (US & Canada)"
			end
		end

end
