# == Schema Information
#
# Table name: wines
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Wine < ActiveRecord::Base
	validates :name, presence: true, uniqueness: true

	has_many :entries
end
