# == Schema Information
#
# Table name: party_invitations
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  party_id   :integer
#  email      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class PartyInvitation < ActiveRecord::Base
	validates_format_of :email, :with => /@/
	validates :party_id, :email, presence: true
	validates_uniqueness_of :email, :scope => [:party_id]
	# Associations
	belongs_to :user
	belongs_to :party
	after_validation :check_for_existing_user


	private

		def check_for_existing_user
			# Determine if User already exists, then associate
			existingUser = User.find_by_email(self.email)
			if !existingUser.nil?
				self.user_id = existingUser.id


				#Determine if user is already a member of the party
				if PartyMembership.where(party_id: self.party_id, user_id: self.user_id).size>0
					errors.add(:email, "User is already part of your party!")
				else
					if PartyInvitation.where(party_id: self.party_id, user_id: self.user_id).size>0
						# Send subsequent invite email
						InvitationMailer.delay.send_invite(self.email,self.party_id)
						errors.add(:email, "Another email has been sent to " + self.email)
					else
						# Send initial invite to existing user.
						InvitationMailer.delay.send_invite(self.email,self.party_id)
						# Record will save
					end
				end
			else
				if PartyInvitation.where(email:self.email, party_id: self.party_id).size>0
					# Send subsequent invite
					InvitationMailer.delay.send_invite(self.email,self.party_id)
					errors.add(:email, "Another email has been sent to " + self.email)
				else
					# Send initial invite to non user
					InvitationMailer.delay.send_invite(self.email,self.party_id)
					# Record will save
				end
			end
		end
end
