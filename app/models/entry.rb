# == Schema Information
#
# Table name: entries
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  party_id    :integer
#  clearname   :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  wine_id     :integer
#  disguise_id :integer
#

class Entry < ActiveRecord::Base
	#Validations
	validates :party_id, :user_id, :clearname, presence: true
	validates_uniqueness_of :party_id, :scope => [:user_id]

	before_create :couple_wine, :attach_disguise_id
	before_update :couple_wine

	#Associations
	belongs_to :user
	belongs_to :party
	has_many :votes, dependent: :destroy
	belongs_to :wine
	belongs_to :disguise

	def self.entry_hash_lookups(party_id)
		# Class method used to create hashes helpful in converting entry_id to code and clear names

		entries_eligible = Entry.where(party_id: party_id).where.not(disguise_id: nil)
		entry_id_to_clearname = Hash[entries_eligible.pluck(:id,:clearname)]
		entry_id_to_codename = Hash.new
		Entry.where(party_id: party_id).each do |entry|
			entry_id_to_codename[entry.id] = entry.codename
		end

		#return object
		hash_lookups = Hash.new

		#Build return object
		hash_lookups["entries_eligible"] = entries_eligible
		hash_lookups["entry_id_to_codename"] = entry_id_to_codename
		hash_lookups["entry_id_to_clearname"] = entry_id_to_clearname
		return hash_lookups
	end
	
	def self.party_members_accepted_invite_no_entry(party_id)
		# Returns emails of party members who have accepted the invite 
		# but have not registered their wines
		party = Party.find(party_id)
		party_members_ids = party.party_memberships.pluck(:user_id)
		party_members_with_entries_ids = party.entries.pluck(:user_id)
		party_members_with_no_entries_ids = party_members_ids - party_members_with_entries_ids
		
		# Return nil if none, makes the render easier.
		party_members_with_no_entries_emails = nil
		if party_members_with_no_entries_ids.length>0
			party_members_with_no_entries_emails = User.where(id: party_members_with_no_entries_ids).pluck(:email)
		end
		return party_members_with_no_entries_emails
	end
	
	def codename
		returnCodename = ""
		if !self.disguise_id.nil?
			returnCodename = self.disguise.name
		end
		return returnCodename
	end
	
	def find_new_disguise_id
		newDisguiseId = attach_disguise_id()
		self.update_attributes(disguise_id: newDisguiseId)
	end

	private

		def couple_wine
			# Use the clearname to find/create a Wine record
			wine_candidate = Wine.find_or_create_by(name: self.clearname)
			self.wine = wine_candidate
		end
		
		def attach_disguise_id
			# When an Entry is first created, it must get assigned a disguise_id
			#   ..potential race condition here, but only between before_create and actual persistence
			disguisesIDsInUse = self.party.entries.pluck(:disguise_id).uniq
			
			disguiseTheme = DisguiseTheme.find(self.party.disguise_theme_id)
			disguiseIDsForPartyTheme = disguiseTheme.disguises.pluck(:id)
			
			if (disguiseIDsForPartyTheme - disguisesIDsInUse).size > 0
				assignedDisguiseID = (disguiseIDsForPartyTheme - disguisesIDsInUse).sample
			else
				# All the disguises have been used for the Party's theme, go to another theme
				
				# Find alternative disguises
				alternativeDisguises = Disguise.where.not(disguise_theme_id: self.party.disguise_theme_id).limit(10).pluck(:id)
				# Remove all currently used disguise_ids (which might include current theme, and other themes)
				alternativeDisguises = alternativeDisguises - disguisesIDsInUse
				# Now sample
				assignedDisguiseID = alternativeDisguises.sample
			end
			
			self.disguise_id = assignedDisguiseID
			# NOte this method doesn't do save/update, only changes instance's disguise_id value.  This value returned for re-use
			return assignedDisguiseID
		end

end
