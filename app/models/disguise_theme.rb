# == Schema Information
#
# Table name: disguise_themes
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class DisguiseTheme < ActiveRecord::Base
    validates :name, presence: true
    validates :name, length: { minimum: 5 }
    validates :name, uniqueness: true
    
    # Associations
    has_many :disguises, dependent: :destroy
end
