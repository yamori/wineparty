class VoteController < ApplicationController
	#Access limitations etc.
	before_action :authenticate_user!
	before_action :find_vote, only: [:update]
	before_action :correct_user_vote, only: [:update]
	before_action :party_concluded_check, only: [:update]
	# Only the party host has access to the dashboard stream
	before_action :correct_user_party_host, only: [:dashboard_ajax]

	def update
		respond_to do |format|
			if !@vote.nil? && @vote.update_attributes(vote_params)
				format.html {
					redirect_to party_path(@vote.party_id)
				}
				format.json {
					render :json => {
						:score => @vote.score,
						:note => @vote.note
					}.to_json, :status => :ok
				}
			else
				format.html {
					flash[:error] = "Something went wrong"
					redirect_to root_path
				}
				format.json {
					render :json => { :error => "Error: unable to save data" }.to_json, :status => 422
				}
			end
		end
	end
	
	def dashboard_ajax
		respond_to do |format|
			vote_with_note_hash = Vote.randomVoteWithNote(params[:id])
        	numbr_votes_with_note = vote_with_note_hash['number_votes_with_note']
			random_vote_with_note = vote_with_note_hash['vote_with_note']
			# Grab the number of shouts
			party_shouts = current_user.parties.find_by_id(params[:id]).shouts
	        number_of_shouts = party_shouts.size
			if !random_vote_with_note.nil?
				vote_note = random_vote_with_note.note
				wine_codename = random_vote_with_note.entry.codename
			else
				vote_note = "Nobody has rated a wine yet!  Hello?!"
				wine_codename = ""
			end
			format.json {
				render :json => { :vote_note => vote_note, :wine_codename => wine_codename, :numbr_shouts => number_of_shouts, 
            		:number_votes_with_note => numbr_votes_with_note }.to_json, :status => :ok
			}
		end
	end

	private
	
		def find_vote
			@vote = Vote.find(params[:id])
			@party = Party.find(@vote.party_id)
		end

		def correct_user_vote
			if @vote.nil? || current_user.id!=@vote.user_id
				flash[:error] = "Something went wrong"
  				redirect_to root_path
			end
		end

		def party_concluded_check
			# Do not allow updating of votes if party.concluded
			if @vote.nil? || @vote.party.concluded
				flash[:error] = "Something went wrong"
  				redirect_to root_path
			end
		end

		def vote_params
			params[:vote].permit(:score,:note)
		end

end