class EntryController < ApplicationController
	#Access limitations etc.
	before_action :authenticate_user!
	before_action :correct_user_entry, only: [:upsert_clearname]

	
	def upsert_clearname
		# User adds/modifies their own Entry.clearname
		respond_to do |format|
			
			upserted_entry = nil
			result = nil
			if params[:entry_id].nil? || params[:entry_id].length==0 || params[:entry_id]=="null"
				# Entry doesn't exist
				upserted_entry = Entry.new(entry_params)
				result = upserted_entry.save ? true : false
			else
				# Find the entry
				upserted_entry = Entry.find(params[:entry_id])
				result = upserted_entry.update(entry_params) ? true : false
			end
			if result
				format.json {
					render :json => { :clearname => upserted_entry.clearname }.to_json, :status => :ok
				}
			else
				format.json {
					render :json => { :clearname => upserted_entry.clearname }.to_json, :status => 422
				}
			end
		end
	end

	private

		def entry_params
			params[:entry].permit(:clearname,:party_id,:user_id)
		end

		def host_modify_entry_params
			params[:entry].permit(:codename)
		end

		def correct_user_entry
			if current_user.id.to_s!=params[:entry][:user_id].to_s || 
				PartyMembership.where(party_id: params[:entry][:party_id], user_id: params[:entry][:user_id]).nil?
				
				#User doesn't correspond to party
				flash[:error] = "Something went wrong"
				redirect_to party_path(params[:entry][:party_id])
			end
		end

end