class PartyController < ApplicationController
	#Access limitations etc.
	before_action :authenticate_user!
	before_action :correct_user_party_show, only: [:show, :state_check, :get_status]
	before_action :correct_user_party_host, only: [:destroy, :update, :conclude]
	before_action :check_if_party_concluded, only: :view_results

	def new
		@party = Party.new
	end

	def create
		@party = current_user.parties.new(party_params)
		if @party.save
			flash[:success] = "Party created!"
			redirect_to :action => 'show', :id => @party.id
		else
			render "new"
		end
	end

	def edit
		@party = current_user.parties.find_by_id(params[:id])
		if !@party
			flash[:error] = "Something went wrong"
			redirect_to root_path
		end
	end

	def update
		@party = current_user.parties.find_by_id(params[:id])
		if !@party.nil? && @party.update_attributes(party_params)
			flash[:success] = "Party updated!"
			redirect_to :action => 'show', :id => @party.id
		else
			flash[:error] = "Something went wrong"
			redirect_to root_path
		end
	end

	def destroy
		@party = current_user.parties.find_by_id(params[:id])
		@party.destroy
		if @party.destroyed?
			flash[:notice] = "Party deleted"
		else
			flash[:error] = "Something went wrong"
		end
		redirect_to root_path
	end
	
	def show
		@party = Party.find(params[:id])
		@invitation = PartyInvitation.new(party_id: params[:id])
		@entry = current_user.find_or_build_entry(params[:id])

		# all_entries used by Host to give codenames
		if current_user.id==@party.user_id
			@invitations = PartyInvitation.where(party_id: params[:id])
			@acceptees = User.where(id: @party.party_memberships.pluck(:user_id))
			
			all_entries_unordered = Entry.where(party_id: params[:id])
			@all_entries = all_entries_unordered.sort { |a,b| a.codename <=> b.codename }
			@users_still_need_entries = Entry.party_members_accepted_invite_no_entry(params[:id])
		else
			@all_entries = nil
			@users_still_need_entries = nil
		end
		
		# For the host
		if @party.concluded
			# Results only matter when party is in concluded state
			@results = Vote.present_results(params[:id], current_user.id)	
		end

		@votes = Vote.retrieve_votes(params[:id],current_user.id)
		@shout = current_user.shouts.new(party_id: params[:id])
	end

	def conclude_flip
		@party = current_user.parties.find_by_id(params[:id])
		
		if !@party.nil?
			# Whenever the concluded flag is flipped, set results_released to false
			msg = @party.flip_concluded
			flash[:notice] = msg
			redirect_to :action => 'show', :id => @party.id
		else
			flash[:error] = "Something went wrong"
			redirect_to root_path
		end
	end
	
	def results_released_flip
		@party = current_user.parties.find_by_id(params[:id])
		
		if !@party.nil?
			@party.update(results_released: !@party.results_released)
			if @party.results_released
				flash[:notice] = "Party Results Released to All"
			else
				flash[:notice] = "Party Results Withdrawn"
			end
			redirect_to :action => 'show', :id => @party.id
		else
			flash[:error] = "Something went wrong"
			redirect_to root_path
		end
	end

	def view_results
		@party = Party.find(params[:id])
		@results = Vote.present_results(params[:id], current_user.id)
		@user_averages = @party.getMembersAvgScores
		@voting_breakdowns = Vote.voting_breakdown(params[:id], current_user.id)
	end
	
	def state_check
		@party = Party.find(params[:id])
		respond_to do |format|
			format.json {
				render :json => {
					:concluded => @party.concluded,
					:results_released => @party.results_released,
					:number_of_entries => @party.entries.where.not(disguise_id: nil).size
				}.to_json, :status => :ok
			}
		end
	end
	
	def get_status
		@party = Party.find(params[:id])
		@stats = Vote.spectate_votes(@party.id)
		respond_to do |format|
			format.json {
				render :json => { :current_number_votes => @stats["current_number_votes"], 
					:max_number_votes => @stats["max_number_votes"],
					:vote_completion => @stats["vote_completion"]}.to_json, :status => :ok
			}
		end
	end

  private 

  	def party_params
		params[:party].permit(:name,:event_date, :disguise_theme_id)
	end

	def check_if_party_concluded
		if !Party.find(params[:id]).concluded
			redirect_to party_path(params[:id])
		end
	end

	def correct_user_party_show
		party_members = PartyMembership.where(party_id: params[:id]).pluck(:user_id)
		if !party_members.include?(current_user.id)
			#User is not a member of the Party
			flash[:error] = "Something went wrong"
  			redirect_to root_path
		end
	end

end

