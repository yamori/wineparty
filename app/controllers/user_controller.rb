class UserController < ApplicationController
    # Contains endpoints not specific to either Devise/Omniauth
    
    #Access limitations etc.
	before_action :authenticate_user!
	before_action :correct_user_forUserUpdates, only: [:update_nickname]
	before_action :correct_user_forUserEditing, only: [:edit, :update, :destroy]
    
    def update_nickname
        respond_to do |format|
            @user = current_user
            if @user.update_attributes(user_params_update_name)
                format.json {
                    render :json => { :tada => "hi!" }.to_json, :status => :ok
                }
            else
                puts "HIt this other logic"
                # format.json { render :json => { :blah => "More than 6 characters in length, please..." }.to_json, :status => 422 }
                format.json { render json: @user.errors, status: :unprocessable_entity }
            end
        end
    end
    
    def edit
    end
    
    def update
        @user = current_user
        if @user.update_attributes(user_params_update)
            flash[:success] = "Account info changed!"
            redirect_to user_edit_path(@user.id)
        else
            flash[:error] = "Something went wrong"
            redirect_to root_path
        end
    end
    
    def destroy
        current_user.destroy
        flash[:notice] = "Account and info deleted"
        redirect_to root_path
    end

    
    private
    
        def correct_user_forUserUpdates
			#Ensure the user who sends the invite is the owner of the Party
			correct_user("/",params[:user][:id])
		end
		
		def correct_user_forUserEditing
		    # Ensure correct user is looking to edit ... the user
		    correct_user("/",params[:id])
		end

        def user_params_update_name
            params[:user].permit(:name)
        end
        
        def user_params_update
            params[:user].permit(:name, :email, :timezone)
        end
    
end