class ApplicationController < ActionController::Base
	helper :all
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private

  	def correct_user(path, id)
  		# Controller wide method to help determine correct user
  		if id.to_s!=current_user.id.to_s
  			flash[:error] = "Something went wrong"
  			redirect_to path
  		end
  	end
  	
  	def correct_user_party_host
		if current_user.parties.find_by_id(params[:id]).nil?
			#Party doesn't belong to current_user
			flash[:error] = "Something went wrong"
			redirect_to root_path
		end
	end
	
end
