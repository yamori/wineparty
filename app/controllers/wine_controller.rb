class WineController < ApplicationController
	#Access limitations etc.
	before_action :authenticate_user!

	def ajax_search
		@wine_names = Wine.order(:name).where("lower(name) LIKE ?", "%#{params[:term].downcase}%").limit(25).map(&:name)
	    if !@wine_names.include?(params[:term])
	      # Include the original search term if it doesn't exist in DB.
	      @wine_names.unshift(params[:term])
	    end
	    respond_to do |format|
	      format.html
	      format.json { 
	        render json: @wine_names
	      }
	    end
	end
end
