class StaticController < ApplicationController
  def index
  	if !current_user.nil?
  		@upcoming_parties = current_user.get_upcoming_parties
  		@invites = current_user.eager_get_invitation_details
  		@previous_parties = current_user.get_previous_parties
  	end
    
  end
end