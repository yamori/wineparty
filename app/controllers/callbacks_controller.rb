class CallbacksController < Devise::OmniauthCallbacksController
	# def facebook
	# 	@user = User.from_omniauth(request.env["omniauth.auth"])
	# 	sign_in_and_redirect @user
	# end
	
	def facebook
		# You need to implement the method below in your model (e.g. app/models/user.rb)
		@user = User.from_omniauth(request.env["omniauth.auth"])
		
		if @user.persisted?
			sign_in_and_redirect @user, :event => :authentication #this will throw if @user is not activated
			if is_navigational_format?
				flash[:success] = "Successfully authenticated with Facebook"
			end
		else
			session["devise.facebook_data"] = request.env["omniauth.auth"]
			redirect_to new_user_registration_url
		end
	end
	
	def failure
		flash[:error] = "Something went wrong with authentication"
		redirect_to root_path
	end
end