class ShoutController < ApplicationController
  
  #Access limitations etc.
	before_action :correct_user_shout, only: [:create]
	# Only the party host has access to the dashboard stream
	before_action :correct_user_party_host, only: [:dashboard_ajax]
  
  def create
    respond_to do |format|
      shout = Shout.new(user_id: current_user.id, party_id: shout_params["party_id"], content: shout_params["content"])
      if shout.save
        format.json {
  				render :json => { }.to_json, :status => :ok
  			}
			else
			  format.json {
  				render :json => { :error => "Error: unable to save data" }.to_json, :status => 422
  			}
			end
    end
  end
  
  def dashboard_ajax
    respond_to do |format|
        party_shouts = current_user.parties.find_by_id(params[:id]).shouts
        random_shout = party_shouts.sample
        number_of_shouts = party_shouts.size
        # Grab the number of votes with note
        vote_with_note_hash = Vote.randomVoteWithNote(params[:id])
        numbr_votes_with_note = vote_with_note_hash['number_votes_with_note']
        if !random_shout.nil?
            shout_content = random_shout.content
        else
            shout_content = "Nobody has said anything yet!  Is anybody out there?!"
        end
        format.json {
            render :json => { :shout_content => shout_content, :numbr_shouts => number_of_shouts, 
                :number_votes_with_note => numbr_votes_with_note }.to_json, :status => :ok
        }
    end
  end
  
  private
  
    def correct_user_shout
			if current_user.id!=shout_params["user_id"].to_i || !current_user.party_memberships.pluck(:party_id).include?(shout_params["party_id"].to_i)
				flash[:error] = "Something went wrong"
  				redirect_to root_path
			end
		end
  
    def shout_params
			params[:shout].permit(:user_id, :party_id, :content)
		end
end
