class PartyInvitationController < ApplicationController
	before_action :authenticate_user!
	before_action :correct_user_partyinvitation, only: [:create]
	before_action :correct_user_for_accept, only: [:accept]

	def create
		@invitation = PartyInvitation.new(partyinvitation_params)
		@invitation.email.downcase!

		# Handle the invite instance, try to save
		if @invitation.save
			@invitation = PartyInvitation.new #For next email to be entered in form
		elsif @invitation.errors[:email].last.include?("already") || @invitation.errors[:email].last.include?("Another")
			#Invitation already exists
			flash.now[:notice] = @invitation.errors[:email].last
			@invitation = PartyInvitation.new #For next email to be entered in form
		else
			flash.now[:error] = "Sorry, the email didn't look valid: \"#{@invitation.email.downcase}\""
		end

		# LIst all invitations outstanding
		@invitations = PartyInvitation.where(party_id: params[:party_invitation][:party_id])
	end

	def accept
		# Accept the invitation
		if current_user.accept_invite(params[:party_id])
			flash[:notice] = "Welcome to the party"
			redirect_to party_path(params[:party_id])
		else
			flash[:notice] = "Something went wrong"
			redirect_to root_url
		end
	end


	private

		def partyinvitation_params
			params[:party_invitation].permit(:email,:party_id)
		end

		def correct_user_partyinvitation
			#Ensure the user who sends the invite is the owner of the Party
			correct_user("/",Party.find(params[:party_invitation][:party_id]).user_id)
		end

		def correct_user_for_accept
			if PartyInvitation.find(params[:id]).user_id!=current_user.id
				flash.now[:error] = "Something went wrong"
				redirect_to root_url
			end
		end

end