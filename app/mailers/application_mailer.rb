class ApplicationMailer < ActionMailer::Base
  default from: "no-reply@grape.social"
  layout 'mailer'
end
