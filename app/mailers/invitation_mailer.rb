class InvitationMailer < ApplicationMailer
	include ApplicationHelper

	before_action :supporting_info

	default from: 'no-reply@grape.social'
 
	def send_invite(inviteeEmail,party_id)
		@party = Party.find(party_id)
		@email = inviteeEmail
		#This should become PartyInvite path for CRUD.
		@url  = root_url
		mail(to: inviteeEmail, subject: "Wine party invitation waiting for you on " + siteName() )
	end

	def supporting_info
		@siteName = siteName()
	end
end
