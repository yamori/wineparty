$(document).on "page:change", ->
  $('#invitations_collapsible').click (event) ->
    event.preventDefault()
    $('html, body').animate { scrollTop: $('#invitations_collapsible').offset().top }, 500
  $('#upcoming_parties_collapsible').click (event) ->
    event.preventDefault()
    $('html, body').animate { scrollTop: $('#upcoming_parties_collapsible').offset().top }, 500
  $('#previous_parties_collapsible').click (event) ->
    event.preventDefault()
    $('html, body').animate { scrollTop: $('#previous_parties_collapsible').offset().top }, 500