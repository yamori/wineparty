// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require jquery-ui
//= require jquery.ui.touch-punch
//= require d3.min
//= require uvcharts.min
//= require materialize-sprockets
//= require general_functions
//= require_tree .

function checkIfMobileWidthBeforeExec(customMessage, targetFunction) {
    // This function is used to check if the view is too small to execute a function
    if ($(window).width() <= 700) {
        // Probably want a nice alert dialog box here, use materialie or something.
        alert(customMessage);
    } else {
        targetFunction();
    }
}