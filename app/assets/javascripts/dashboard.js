// Vars for Social Dashboard
var dashboardRefreshRate = 10000;
var fadeInOutMS = 1000;
var dashboardTextTicketTimeout = null;
var dashboardCountDownTimeout = null;

// Initial distribution
var numberOfShouts = 1;
var numberOfVotesWithNote = 1;

function dashboardShoutAjax(party_id) {
    // Detect if the the user has navigated away. (Covers new http request or turbolinks ajax)
    if ( $( "#socialDashboardContent" ).length==0 ) {
        clearTimeout(dashboardTextTicketTimeout);
        clearTimeout(dashboardCountDownTimeout);
        return;
    }
    
    // Build the distribution array, sample, and this will decide to get either a SHout or a Vote
    var distributionArr = [];
    for (i = 0; i < numberOfShouts; i++) { 
	    distributionArr.push(1); // Shout
	}
	for (i = 0; i < numberOfVotesWithNote; i++) { 
	    distributionArr.push(2); // Shout
	}
	var outcome = distributionArr[Math.floor(Math.random() * distributionArr.length)];

	// Based on the outcome from the distribution, query for either Shour or Vote with note
	if ( outcome==1 ) {
		$.ajax({
			type: "GET",
			callback: "JSON",
			dataType: "json",
			url: '/shout/' + party_id + '/dashboard_ajax.json',
			success: function(msg) {
				// Update the counters
				numberOfShouts = Math.max(1,msg.numbr_shouts);
				numberOfVotesWithNote = Math.max(1,msg.number_votes_with_note);
				// Update the html
				$( "#socialDashboardContent" ).fadeOut(fadeInOutMS, "linear", function() {
					$("#dashboard_first_p").html("Shout!");
					$("#dashboard_second_p").html("\"" + msg.shout_content + "\"");
					$( "#socialDashboardContent" ).fadeIn(fadeInOutMS);
				});
			},
			error: function(msg) {
				alert( "Hmm, something went wrong." );
			}
		});
	} else {
		$.ajax({
			type: "GET",
			callback: "JSON",
			dataType: "json",
			url: '/vote/' + party_id + '/dashboard_ajax.json',
			success: function(msg) {
				// Update the counters
				numberOfShouts = Math.max(1,msg.numbr_shouts);
				numberOfVotesWithNote = Math.max(1,msg.number_votes_with_note);
				// Update the html
				$( "#socialDashboardContent" ).fadeOut(fadeInOutMS, "linear", function() {
					$("#dashboard_first_p").html("Someone rated wine '<b>" + msg.wine_codename + "</b>'");
					$("#dashboard_second_p").html("\"" + msg.vote_note + "\"");
					$( "#socialDashboardContent" ).fadeIn(fadeInOutMS);
				});
			},
			error: function(msg) {
				alert( "Hmm, something went wrong." );
			}
		});
	}
	
	// Update the refresh rate
	dashboardRefreshRate = parseInt($( "#dashboard-freqq" ).val());
	
    // 	Continue the loop, make a TimeOut for another ajax call.  Does not end.
	clearTimeout(dashboardTextTicketTimeout);
	dashboardTextTicketTimeout = setTimeout(function(){ 
		dashboardShoutAjax(party_id);
	},dashboardRefreshRate);
	
	// Refresh the countdown timer
	clearTimeout(dashboardCountDownTimeout);
	dashBoardCountdown(dashboardRefreshRate/1000);
}

function dashBoardCountdown(freq_sec) {
	if (freq_sec<=0) {
		clearTimeout(dashboardCountDownTimeout);
		freq_sec = dashboardRefreshRate/1000;
	}
	// Counts down to zero
	var refreshRate = 1; //1 second
	$("#dashboard-countdown").html(freq_sec);
	clearTimeout(dashboardCountDownTimeout);
	dashboardCountDownTimeout = setTimeout(function(){ 
		dashBoardCountdown(freq_sec - refreshRate);
	}, refreshRate*1000); //Strictly 1s
}
