
// All ajax related for the Shouts

var shoutTimeout = null;

function onShoutChange() {
	$("#shout_out_div" ).removeClass('background_changes_made');
	$("#shout_out_div" ).removeClass('backgroundAnimated_success');
	$("#shout_out_div" ).removeClass('background_failure');
	clearTimeout( shoutTimeout );
	shoutTimeout = setTimeout(function(){ 
		$("#shout_out_div").addClass('background_changes_made');
		$("#shout_text_area_save").removeClass('disabled');
		$("#shout_text_area_cancel").removeClass('disabled');
	},500);
}

function clearShoutOUt() {
    $("#shout_text_area").val('');
    $("#shout_out_div" ).removeClass('background_changes_made');
	$("#shout_out_div" ).removeClass('backgroundAnimated_success');
	$("#shout_out_div" ).removeClass('background_failure');
    $("#shout_text_area_save").addClass('disabled');
	$("#shout_text_area_cancel").addClass('disabled');
}

function submitShoutOut() {
	$("#shout_out_div" ).removeClass('background_changes_made');
	$("#shout_out_div" ).removeClass('backgroundAnimated_success');
	$("#shout_out_div" ).removeClass('background_failure');
	$.ajax({
		type: "POST",
		callback: "JSON",
		dataType: "json",
		url: '/shout/create.json',
		data: { _method:'PUT', shout: { content : $("#shout_text_area").val(), user_id : $("#shout_user_id").val(), party_id : $("#shout_party_id").val() } },
		success: function(msg) {
		    $("#shout_text_area").val('');
			$("#shout_text_area_save").addClass('disabled');
	        $("#shout_text_area_cancel").addClass('disabled');
			$("#shout_out_div" ).addClass('backgroundAnimated_success');
		},
		error: function(msg) {
			$("#shout_text_area").val('');
			$("#shout_text_area_save").addClass('disabled');
	        $("#shout_text_area_cancel").addClass('disabled');
			$("#shout_out_div" ).addClass('background_failure');
			alert( "Hmm, something went wrong." );
		}
	});
}
