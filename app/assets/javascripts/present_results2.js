function toggleContentByID(divID) {
    $('#' + divID).toggle(function() {
        // Prevents default behavior and unwanted scrolling
        return false;
    });
    
    // if ( $('#' + divID).is(":visible") ) {
    //     console.log("did the scroll logic");
    //     $('html, body').animate({
    //         scrollTop: $('#' + divID).offset().top - 50
    //     }, 800);
    // }
    return false; // Preent default behavior.
}

function toggleContentByIDAndHideShow(divID, hideID, showID) {
    // Similar to method above but also hid and show content at same time.
    $('#' + divID).toggle(function() {
        // Prevents default behavior and unwanted scrolling
        return false;
    });
    $('#' + showID).fadeIn(1000);
    $('#' + hideID).hide();
    
    // if ( $('#' + divID).is(":visible") ) {
    //     console.log("did the scroll logic");
    //     $('html, body').animate({
    //         scrollTop: $('#' + divID).offset().top - 50
    //     }, 800);
    // }
    return false; // Preent default behavior.
}