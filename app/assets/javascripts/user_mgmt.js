// All Ajax and logic meant for managin a user
function submitNameChange(user_id) {
	$.ajax({
		type: "POST",
		callback: "JSON",
		dataType: "json",
		url: '/user/update/' + user_id +'.json',
		data: { _method:'PUT', user: { id : user_id, name : $("#current_user_name_text_area").val() } },
		success: function(msg) {
			//alert("All set!");
			// THis logic will then close the modal.  Maybe take away the object above, msg.tada.
			$('#username_modal').closeModal();
		},
		error: function(msg) {
			// Unfortunatley have to dig through the return object for model validation error message
			var array_values = new Array();
			for (var key in msg.responseJSON) {
			    array_values.push(msg.responseJSON[key]);
			}
			//console.log(msg.responseJSON);
			alert(array_values);
			$('#current_user_name_text_area').focus();
		}
	});
}