function zoomModalContent(zoomFactor, parentElement) {
  var elementsToZoom = ["h2","h3","h4","h5","p"]
  for (var i = 0; i < elementsToZoom.length; i++) {
    // Adjust font size
    var fontInPixels = parseFloat( $(parentElement + " " + elementsToZoom[i]).css("font-size") );
    var newFontInPixels = zoomFactor * fontInPixels;
    $(parentElement + " " + elementsToZoom[i]).css("font-size", newFontInPixels + "px");
    
    // Adjust top and bottom margin
    if (elementsToZoom[i] != "p") {
      var topMarginInPixels = parseFloat( $(parentElement + " " + elementsToZoom[i]).css("margin-top") );
      var newtopMarginInPixels = zoomFactor * topMarginInPixels;
      $(parentElement + " " + elementsToZoom[i]).css("margin-top", newtopMarginInPixels + "px");
      
      var bottomMarginInPixels = parseFloat( $(parentElement + " " + elementsToZoom[i]).css("margin-bottom") );
      var newBottomMarginInPixels = zoomFactor * bottomMarginInPixels;
      $(parentElement + " " + elementsToZoom[i]).css("margin-bottom", newBottomMarginInPixels + "px");
      
    }
  }
}