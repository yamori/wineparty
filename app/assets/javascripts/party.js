// Async vars used for submitting entry
var entryTimeout = null;
var previousClearname = "";

$(document).on('page:load ready', function() {
  $("#party_event_date").datepicker({
    dateFormat: 'yy-mm-dd'
  });
  $('.button-collapse').sideNav();
  $('.collapsible').collapsible({
    accordion: false
  });
});

// Auto Complete for wine choice.
function autoUpdateWineQuery() {
  $("#clearname_textfield2").autocomplete({
    source: '/wine/ajax_search.json',
    select: function( event, ui ) { 
      onEntryChange();
    },
    open: function (result) {
        if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
            $('.ui-autocomplete').off('menufocus hover mouseover');
        }
    },
    position: { my : "left bottom", at: "left top" },
    minLength: 3
  });
}

function onEntryChange() {
  // Reset clearname_submit_btn
  clearTimeout(entryTimeout);
  $("#div_clearname_form").removeClass("background_changes_made");
  $("#div_clearname_form").removeClass("backgroundAnimated_success");
  $("#clearname_submit_btn").addClass("disabled");
  $("#clearname_cancel_btn").addClass("disabled");
  
  // Make changes
  entryTimeout = setTimeout(function(){ 
    $("#div_clearname_form").addClass("background_changes_made");
    $("#clearname_submit_btn").removeClass("disabled");
    $("#clearname_cancel_btn").removeClass("disabled");
  },500);
}

function submitEntryUpsert(entry_id, user_id, party_id) {
  // Reset
  $("#div_clearname_form").removeClass("background_changes_made");
  $("#div_clearname_form").removeClass("backgroundAnimated_success");
  // Makes the ajax call to upsert the user's Entry
  $.ajax({
		type: "POST",
		callback: "JSON",
		dataType: "json",
		url: '/entry_upsert_clearname/'+entry_id+'.json',
		data: { _method:'PUT', 
		  entry: { 
		    user_id : user_id, party_id : party_id, clearname : $("#clearname_textfield2").val() 
		  } ,
		    entry_id: entry_id
		  },
		success: function(msg) {
			$("#clearname_submit_btn").addClass('disabled');
			$("#clearname_cancel_btn").addClass('disabled');
			$("#div_clearname_form").addClass('backgroundAnimated_success');
			$("#clearname_textfield2").val(msg.clearname);
			previousClearname = msg.clearname;
			// Hide the reminder to register
			$("#register_reminder").hide();
		},
		error: function(msg) {
		  $("#clearname_submit_btn").addClass('disabled');
			$("#clearname_cancel_btn").addClass('disabled');
			$("#div_clearname_form").addClass('background_changes_made');
			$("#clearname_textfield2").val(msg.clearname);
		// 	reverUserEntryValue();
			alert( "Unable to update your wine.  Try again later please." );
			window.location.reload();
		}
	});
}

function reverUserEntryValue() {
  // Reset
  $("#div_clearname_form").removeClass("background_changes_made");
  $("#div_clearname_form").removeClass("backgroundAnimated_success");
  
  // Make the change.
  $("#clearname_textfield2").val(previousClearname);
  
  // UI
  $("#clearname_submit_btn").addClass('disabled');
	$("#clearname_cancel_btn").addClass('disabled');
  $("#div_clearname_form").addClass("backgroundAnimated_success");
}