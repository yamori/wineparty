// Party page refresh logic: if the host has changed the state of the party, this endpoint will help refresh the attendees' page

/* global partyConcluded, partyNumberOfEntries, partyResultsReleased, partyPageStateCheckInterval */

function partyPageStateCheck(id) {
	$.ajax({
		type: "GET",
		callback: "JSON",
		dataType: "json",
		url: '/party/state_check/' + id + '.json',
		data: { _method:'GET', id: id },
		success: function(msg) {
		    // Check if the state has changed, or if another disguised wine has been added
		    if ((partyConcluded!=msg.concluded) || (partyResultsReleased!=msg.results_released) || (partyNumberOfEntries!=msg.number_of_entries)) {
		        // As soon as change is detected, give the user the footer notice, and stop the interval
		        clearInterval(partyPageStateCheckInterval);
		        $( "#footer-buffer" ).show( "slow" );
		        $( "#footer" ).show( "slow" );
		    }
		},
		error: function(msg) {
		    // do nothing
		}
	});
}