// Async call for updating Vote
var originalVoteScoreByID = {};
var originalVoteNoteByID = {};
var timeoutMap = {};

// New for the improved AJAX voting
function onVoteChange(id) {
	$("#vote_tr_".concat(id)).removeClass('background_changes_made');
	clearTimeout(timeoutMap['vote_score_'+id]);
	timeoutMap['vote_score_'+id] = setTimeout(function(){ 
		$("#vote_tr_".concat(id)).addClass('background_changes_made');
		$("#vote_save_btn_".concat(id)).removeClass('disabled');
		$("#vote_cancel_btn_".concat(id)).removeClass('disabled');
	},500);
}

function submitVoteChange(id) {
	$("#vote_tr_".concat(id)).removeClass('background_changes_made');
	$("#vote_tr_".concat(id)).removeClass('backgroundAnimated_success');
	$.ajax({
		type: "POST",
		callback: "JSON",
		dataType: "json",
		url: '/vote/'+id+'.json',
		data: { _method:'PUT', vote: { score : $("#vote_score_".concat(id)).val(), note : $("#vote_note_".concat(id)).val() } },
		success: function(msg) {
			$("#vote_save_btn_".concat(id)).addClass('disabled');
			$("#vote_cancel_btn_".concat(id)).addClass('disabled');
			$("#vote_tr_".concat(id)).addClass('backgroundAnimated_success');
			updateVoteFormWithAjaxMsg(msg, id);
		},
		error: function(msg) {
			$("#vote_tr_".concat(id)).addClass('background_failure');
			revertUserFormValues(id);
			alert( "Voting has been concluded, hold fast for the results!" );
			window.location.reload();
		}
	});
}

function revertVoteChange(id) {
	$("#vote_tr_".concat(id)).removeClass('background_changes_made');
	$("#vote_tr_".concat(id)).removeClass('backgroundAnimated_success');
	revertUserFormValues(id);
	$("#vote_save_btn_".concat(id)).addClass('disabled');
	$("#vote_cancel_btn_".concat(id)).addClass('disabled');
	$("#vote_tr_".concat(id)).addClass('backgroundAnimated_success');
}

function updateVoteFormWithAjaxMsg(msg, id) {
	$("#vote_score_".concat(id)).val(msg.score);
	$("#vote_note_".concat(id)).val(msg.note);
	// Update the array keeping track of original scores.
	originalVoteScoreByID[String(id)]=msg.score;
	originalVoteNoteByID[String(id)]=msg.note;
}

function revertUserFormValues(id) {
	$("#vote_score_".concat(id)).val(originalVoteScoreByID[String(id)]);
	$("#vote_note_".concat(id)).val(originalVoteNoteByID[String(id)]);
}