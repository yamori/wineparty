module ApplicationHelper

	def siteName()
		return "Grape.Social"
	end
	
	def getUsersTimeZone()
		return current_user.timezone rescue "Eastern Time (US & Canada)"
	end
end
