# README #
## WineParty ##

Ruby on Rails app for socializing a single-blind wine tasting party.

### Versions ###

* ruby 2.1.5p273
* Rails 4.2.0
* Dev/Test DB: Postgres 9.3.0

### How do I get set up? ###

Target Dev environment is Mac OS Yosemite.  

* rvm for Ruby
* Postgres
* (anything else?)
* Environment variables
*   export PARTY_PAGE_REFRESH_RATE=3000

### Dev Pointers ###
* Dev/testing data
  * rake db:populate.  then login: abc@gmail.com;password
* To run dev env locally
  * First terminal
    * set env var for RUBY_PASSWORD_DEV
    * forman start
  * Second terminal
    * rails server
* To preview an email: http://localhost:3000/rails/mailers/invitation_mailer/send_invite
* Seed data: rake db:upsert_disguises_and_themes

### CLoud9 ###

Currently there are issues for runnign Rails server, run these commands after firing up the cloud9 workspace
* rvm use ruby-2.1.5
* bundle install
* sudo service postgresql start
* rails s -p $PORT -b $IP

Then proceed as usual.  Sample user is: then login: abc@gmail.com;password.

### Agile / Issues / Wiki ###

Issues will be tracked on bitbucket.  Scrum board will be hosted somewhere else.

### Authentication ###
* Using Device and OmniAuth
* Facebook: for dev & test only, devise.rb not setup for PROD


Running Dev:
	-First terminal
		-Set env var for RUBY_PASSWORD_DEV
		-foreman start
	-Second terminal
		-rails s

	
Function Testing Circuit:
-Root page
-New User
-Root page
-Login
-Create Party
-User Page
-Select Party
-Conclude Party
-Present Results


