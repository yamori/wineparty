namespace :db do
	desc "Fill database with sample data (for dev/test only, no seed)"
	task :populate => :environment do
		#Useful for viewing test dev data.
		require 'faker'
		require 'factory_girl_rails'

		Rake::Task['db:reset'].invoke
		
		# Seed the Disguise themes and table
		Rake::Task['db:upsert_disguises_and_themes'].invoke

		#Create the host
		User.create(email:'abc@gmail.com',password: FactoryGirl.build(:user).password, 
			password_confirmation: FactoryGirl.build(:user).password)
		host_id = User.first.id
			
		# Future guests
		9.times do |n|
			User.create(email: Faker::Internet.email,password: FactoryGirl.build(:user).password, 
				password_confirmation: FactoryGirl.build(:user).password)
		end
		guest_ids = User.all.pluck(:id)
		guest_ids.delete(host_id)

		#Create the party
		FactoryGirl.create(:party, :user_id => host_id, :name => "PartyOfInterest", event_date: Date.today.strftime("%Y") + "-12-31")
		partyID = Party.first.id
		#Create all memberships
		guest_ids.each do |userID|
			FactoryGirl.create(:party_membership, user_id: userID, party_id: partyID)
		end

		#Create the entries
		User.all.pluck(:id).each do |userID|
			FactoryGirl.create(:entry, party_id: partyID, user_id: userID, clearname: Faker::Company.name)
		end

		#Create the votes
		User.where.not(id: host_id ).pluck(:id).each do |userID|
			Entry.all.pluck(:id).each do |entryID|
				FactoryGirl.create(:vote, party_id: partyID, user_id: userID, entry_id: entryID, score: rand(11), note: Faker::Lorem.sentence(3, true, 12))
			end
		end


		# Create other parties:
		guest_ids.each do |userID|
			# Member of a party
			FactoryGirl.create(:party, :user_id => userID, event_date: rand(2000..2030).to_s + "-01-31")
			FactoryGirl.create(:party_membership, user_id: host_id, party_id: Party.last.id)
			# Invited to a party
			FactoryGirl.create(:party, :user_id => userID, event_date: rand(2020..2030).to_s + "-01-31")
			FactoryGirl.create(:party_invitation, :email => User.find(host_id).email, :user_id => host_id, :party_id => Party.last.id)
		end
		
		# Create a bunch of shout outs
		User.all.each do |userN|
			userN.party_memberships.each do |partyMembership|
				userN.shouts.create(content: Faker::Lorem.sentence, party_id: partyMembership.party_id)
			end
		end
		puts "Created #{Shout.all.size} shouts"



	end

end