namespace :db do
	desc "Upserts (find_or_create_by) Disguise and DisguiseThme with appropriate data"
	task :upsert_disguises_and_themes => :environment do
	    # IDs are explicitly named here to maintain the upsert strategy
	    
	    
	    natoTheme_name = "Nato Alphabet"
	    natoTheme = DisguiseTheme.find_or_create_by(name: natoTheme_name)
	    natoTheme_arr = [
            "Alpha",
            "Bravo",
            "Charlie",
            "Delta",
            "Echo",
            "Foxtrot",
            "Golf",
            "Hotel",
            "India",
            "Juliett",
            "Kilo",
            "Lima",
            "Mike",
            "November",
            "Oscar",
            "Papa",
            "Quebec",
            "Romeo",
            "Sierra",
            "Tango",
            "Uniform",
            "Victor",
            "Whiskey",
            "X-ray",
            "Yankee",
            "Zulu" ]
        natoTheme_arr.each do |disguise|
            Disguise.find_or_create_by(name: disguise, disguise_theme_id: natoTheme.id)
        end
        if (natoTheme.disguises.size!=natoTheme_arr.size)
            puts "Discrepancy in Disguise populatate data: " + natoTheme_name
        else
            puts "Theme populated: #{natoTheme_name}, records #{natoTheme.disguises.size}"
        end
        
        
        
        usCities_name = "U.S. Cities"
	    usCities_Theme = DisguiseTheme.find_or_create_by(name: usCities_name)
	    usCities_Theme_arr = [
            "Boston",
            "New York",
            "Philadelphia",
            "Baltimore",
            "Pittsburgh",
            "Chicago",
            "Miami",
            "Dallas",
            "Houston",
            "Las Vegas",
            "Los Angeles",
            "San Francisco",
            "Seattle",
            "Austin",
            "New Orleans",
            "Nashville",
            "Columbus",
            "Buffalo",
            "Cincinnati",
            "Cleveland",
            "Phoenix",
            "Tulsa",
            "Denver",
            "Tucson",
            "Sacramento" ]
        usCities_Theme_arr.each do |disguise|
            Disguise.find_or_create_by(name: disguise, disguise_theme_id: usCities_Theme.id)
        end
        if (usCities_Theme.disguises.size!=usCities_Theme_arr.size)
            puts "Discrepancy in Disguise populatate data: " + usCities_name
        else
            puts "Theme populated: #{usCities_name}, records #{usCities_Theme.disguises.size}"
        end
        
        
        
        
        euroCountries_name = "European Countries"
	    euroCountries_Theme = DisguiseTheme.find_or_create_by(name: euroCountries_name)
	    euroCountries_Theme_arr = [
            "Sweden",
            "Greece",
            "Norway",
            "Finland",
            "Denmark",
            "Iceland",
            "France",
            "England",
            "Germany",
            "Spain",
            "Portugal",
            "Italy",
            "Austria",
            "Switzerland",
            "Poland",
            "Croatia",
            "Luxembourg",
            "Bulgaria",
            "Belgium",
            "Netherlands",
            "Estonia",
            "Ireland" ]
        euroCountries_Theme_arr.each do |disguise|
            Disguise.find_or_create_by(name: disguise, disguise_theme_id: euroCountries_Theme.id)
        end
        if (euroCountries_Theme.disguises.size!=euroCountries_Theme_arr.size)
            puts "Discrepancy in Disguise populatate data: " + euroCountries_name
        else
            puts "Theme populated: #{euroCountries_name}, records #{euroCountries_Theme.disguises.size}"
        end
        
        
        
        usPrezTheme_name = "U.S. Presidents"
	    usPrez_Theme = DisguiseTheme.find_or_create_by(name: usPrezTheme_name)
	    usPrez_Theme_arr = [
            "Barack Obama",
            "George W. Bush",
            "Bill Clinton",
            "George H. W. Bush",
            "Jimmy Carter",
            "Ronald Reagan",
            "Gerald Ford",
            "Richard Nixon",
            "Lyndon Johnson",
            "John F. Kennedy",
            "Dwight Eisenhower",
            "Franklin D. Roosevelt",
            "Herbert Hoover",
            "Calvin Coolidge",
            "Warren Harding",
            "Woodrow Wilson",
            "William Howard Taft",
            "Theodore Roosevelt",
            "William McKinley",
            "Grover Cleveland",
            "Benjamin Harison",
            "Chester A. Arthur",
            "James A. Garfield",
            "Rutherford B. Hayes",
            "Ulysses S. Grant",
            "Andrew Johnson",
            "Abraham Lincoln",
            "James Buchanan",
            "Franklin Pierce",
            "Millard Fillmore",
            "Zachardy Tyler",
            "James Polk",
            "John Tyler",
            "William Harrison",
            "Martin Van Buren",
            "John Quincy Adams",
            "James Munroe",
            "James Madison",
            "Thomas Jefferson",
            "John Adams",
            "George Washington" ]
        usPrez_Theme_arr.each do |disguise|
            Disguise.find_or_create_by(name: disguise, disguise_theme_id: usPrez_Theme.id)
        end
        if (usPrez_Theme.disguises.size!=usPrez_Theme_arr.size)
            puts "Discrepancy in Disguise populatate data: " + usPrezTheme_name
        else
            puts "Theme populated: #{usPrezTheme_name}, records #{usPrez_Theme.disguises.size}"
        end
        
        
        
        sirenTheme_name = "Sirens"
	    siren_Theme = DisguiseTheme.find_or_create_by(name: sirenTheme_name)
	    siren_Theme_arr = [
            "Meara",
            "Evie",
            "Emmeline",
            "Tamsyn",
            "Bryn",
            "Beatrix",
            "Lorraine",
            "Nerobel",
            "Irina",
            "Edolie",
            "Saskia",
            "Nicasia",
            "Marisol",
            "Faustina",
            "Sosina",
            "Celine",
            "Cressida",
            "Allira",
            "Aletta",
            "Dardana",
            "Blair",
            "Calypso",
            "Lamia" ]
        siren_Theme_arr.each do |disguise|
            Disguise.find_or_create_by(name: disguise, disguise_theme_id: siren_Theme.id)
        end
        if (siren_Theme.disguises.size!=siren_Theme_arr.size)
            puts "Discrepancy in Disguise populatate data: " + sirenTheme_name
        else
            puts "Theme populated: #{sirenTheme_name}, records #{siren_Theme.disguises.size}"
        end
        
        
        
        
        hunkTheme_name = "Hunks"
	    hunk_Theme = DisguiseTheme.find_or_create_by(name: hunkTheme_name)
	    hunk_Theme_arr = [
            "Channing",
            "Arturo",
            "Aziel",
            "Bradford",
            "Bryce",
            "Brion",
            "Byron",
            "Carrick",
            "Colt",
            "Clive",
            "Daire",
            "Dante",
            "Darius",
            "Dax",
            "Gerard",
            "Hudson",
            "Kento",
            "Kirk",
            "Nikhil",
            "Rafael",
            "Ramon",
            "Stefan",
            "Tristian",
            "Zane" ]
        hunk_Theme_arr.each do |disguise|
            Disguise.find_or_create_by(name: disguise, disguise_theme_id: hunk_Theme.id)
        end
        if (hunk_Theme.disguises.size!=hunk_Theme_arr.size)
            puts "Discrepancy in Disguise populatate data: " + hunkTheme_name
        else
            puts "Theme populated: #{hunkTheme_name}, records #{hunk_Theme.disguises.size}"
        end
	    
	    
	end
end
