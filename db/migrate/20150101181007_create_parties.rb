class CreateParties < ActiveRecord::Migration
  def change
    create_table :parties do |t|
      t.string :name
      t.datetime :event_date
      t.string :event_key

      t.timestamps null: false
    end
  end
end
