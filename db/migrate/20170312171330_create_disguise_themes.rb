class CreateDisguiseThemes < ActiveRecord::Migration
  def change
    create_table :disguise_themes do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
