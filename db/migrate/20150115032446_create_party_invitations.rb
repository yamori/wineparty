class CreatePartyInvitations < ActiveRecord::Migration
  def change
    create_table :party_invitations do |t|
      t.integer :user_id
      t.integer :party_id
      t.string :email

      t.timestamps null: false
    end
  end
end
