class RemoveEligibleForVoteFromEntries < ActiveRecord::Migration
  def change
    remove_column :entries, :eligible_for_vote, :boolean
  end
end
