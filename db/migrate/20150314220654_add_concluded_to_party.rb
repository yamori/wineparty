class AddConcludedToParty < ActiveRecord::Migration
  def change
    add_column :parties, :concluded, :boolean
  end
end
