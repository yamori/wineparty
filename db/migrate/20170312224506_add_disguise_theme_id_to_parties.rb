class AddDisguiseThemeIdToParties < ActiveRecord::Migration
  def change
    add_column :parties, :disguise_theme_id, :integer
  end
end
