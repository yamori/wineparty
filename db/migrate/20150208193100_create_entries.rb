class CreateEntries < ActiveRecord::Migration
  def change
    create_table :entries do |t|
      t.integer :user_id
      t.integer :party_id
      t.string :clearname
      t.string :codename

      t.timestamps null: false
    end
  end
end
