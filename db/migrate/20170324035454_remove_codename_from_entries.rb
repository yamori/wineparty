class RemoveCodenameFromEntries < ActiveRecord::Migration
  def change
    remove_column :entries, :codename, :string
  end
end
