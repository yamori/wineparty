class CreateDisguises < ActiveRecord::Migration
  def change
    create_table :disguises do |t|
      t.string :name
      t.integer :disguise_theme_id

      t.timestamps null: false
    end
  end
end
