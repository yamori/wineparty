class AddEligibleForVoteToEntry < ActiveRecord::Migration
  def change
    add_column :entries, :eligible_for_vote, :boolean
  end
end
