class AddWineIdToEntry < ActiveRecord::Migration
  def change
    add_column :entries, :wine_id, :integer
  end
end
