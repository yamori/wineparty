class AddDisguiseIdToEntries < ActiveRecord::Migration
  def change
    add_column :entries, :disguise_id, :integer
  end
end
