class CreateShouts < ActiveRecord::Migration
  def change
    create_table :shouts do |t|
      t.integer :user_id
      t.integer :party_id
      t.string :content

      t.timestamps null: false
    end
  end
end
