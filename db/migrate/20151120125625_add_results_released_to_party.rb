class AddResultsReleasedToParty < ActiveRecord::Migration
  def change
    add_column :parties, :results_released, :boolean
  end
end
