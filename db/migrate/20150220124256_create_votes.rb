class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.integer :user_id
      t.integer :party_id
      t.integer :entry_id
      t.integer :score
      t.string :note

      t.timestamps null: false
    end
  end
end
